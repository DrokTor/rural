import fr from "./fr.json";
import en from "./en.json";
import de from "./de.json";

export default { fr, en, de };
