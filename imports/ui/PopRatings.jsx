import React, { Component } from 'react';
import { Query } from "react-apollo";
import gql from "graphql-tag";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
//import Icon from '@material-ui/core/Icon';
import {Link} from "react-router-dom";
//import {DeleteIcon} from '@material-ui/icons/DeleteIcon';
import {Delete , ShoppingCart, AddShoppingCart, RemoveShoppingCart} from '@material-ui/icons';
import Rating from 'react-rating';
import { injectIntl, defineMessages } from "react-intl";

const messages = defineMessages({
  title: {
    id: 'PopProducts.title',
    defaultMessage: 'Les plus populaires'
  }
})

class PopProducts extends Component {

  constructor(props){
    super(props);

    // this.state={
    //   shopList: [],
    // }
    //
    // this.handleWish = this.handleWish.bind(this);
  }
  //
  // handleWish(elem){
  //
  //     let wishList = this.state.wishList;
  //     //const elem = event.target.dataset.id;
  //     if(wishList.indexOf(elem) === -1){
  //       wishList.push(elem);
  //       console.log('aded');
  //       console.log(elem);
  //     }else {
  //       wishList.splice(wishList.indexOf(elem), 1);
  //       console.log('removed');
  //     }
  //     this.setState({wishList},()=>{
  //
  //        // call handle with from parent too to update header
  //     });
  // }



  render(){
    const {intl:{formatMessage}} = this.props;

    const GET_RATINGS = gql`
      query getRatings($id: [ID], $limit: Int = 6 ){
        getRatings(_id: $id, limit: $limit)  {
          _id
          product_id
          rating
          title
          comment
        }
      }
    `;


    // let variables = {};
    // if(!this.props.showShopList && this.props.showWishList && this.props.showWishList.length>0){
    //   variables.id  = this.props.showWishList ;
    // }


    return <div className="row col-12  p-5 mx-auto mt-2 pt-6">
      <h5 className="row col-12 mb-3" >{formatMessage(messages.title)}</h5>
      {
        this.props.showShopList && <div className="row col-12 mb-5 ">
          <div className="row col-9 p-4 border">
            <div className="col-3">Total:</div>
            <label className="col-9">{this.props.cost+" € "}</label>
          </div>
          <button className="btn btn-warning col-3   p-3 pb-4" onClick={this.props.handleOrder}>COMMMANDER <i className="fa fa-cart-arrow-down fa-2x text-white"></i></button>
          </div>
      }
     <Query
      query={GET_RATINGS}
      variables={variables}
      //variables={{id : ["CJ2gth3WxgyQx2DCv"]  }}
      //pollInterval={2000}
      >
      {({ loading, error, data, variables }) => {
        // console.log(variables);
        if (loading) return <p>Loading...</p>;
        if (error) return <p>Error :(</p>;
        //random image remove for product
        let url = Meteor.absoluteUrl();
        url = url.split('://');
        url[1] = url[1].split(':');
        const baseurl = url[1][0].replace(/\/$/, ""); // remove trailing slash;

        url  = url[0]+"://"+baseurl+':3080/';


        return data.getProducts.map((product,index) => {
          //let img= "/assets/images/"+Math.floor((Math.random() * 4) + 1)+".jpeg";
          //console.log(product);
    return <div key= {index} className="col-2 mb-5 ">
            <Link to={"/product/"+product._id} className="no-deco">
            <div className="item-container row col-12 m-0 p-0 pointer border" >
              <div className="position-relative image-container2 d-flex align-items-center">
                <img className="p-0 col-12 " src={url+product._id+"/"+product.filenames[0]}/>
              <div  className="col-12   heart-container2">

                {/* <i data-id={product._id} className={"float-right fa   fa-wish pointer  "+(this.props.wishList.indexOf(product._id)=== -1 ? " fa-heart-o":" fa-heart wish-color" )} onClick={event => {event.preventDefault(); this.props.handleWish(event.target.dataset.id)}}></i> */}
                <Query query={GET_WISH_SHOPLIST}>
                  {({ data, client }) => {



                    return(
                      <div className="float-right d-flex align-items-center justify-content-center">
                      <i data-id={product._id}
                         className={" fa   fa-wish pointer  "+(data.wishList && data.wishList.indexOf(product._id) != -1 ? " fa-heart wish-color":" fa-heart-o " )}
                       onClick={e =>{
                         e.preventDefault();
                          this.props.handleWish(client,data,product._id) }}
                       ></i>
                       </div>
                     )
                  }}
                </Query>

                {/* <i data-id={product._id} className= {"col text-right fa  fa-2x pointer float-right"+(this.props.shopList.indexOf(product._id)=== -1? " fa-shopping-cart ":" fa-cart-arrow-down cart-color")}  onClick={event => {event.preventDefault(); this.props.handleShop(event.target.dataset.id,product.price)}}></i> */}

                {/* {
                  this.props.shopList.indexOf(product._id)=== -1?
                  <i data-id={product._id} onClick={event => this.props.handleShop(event.target.dataset.id)}><AddShoppingCart  className="" fontSize="large"   /></i>
                  :
                  <i data-id={product._id} onClick={event => this.props.handleShop(event.target.dataset.id)}><RemoveShoppingCart  className="" fontSize="large"  /></i>
                } */}



              </div>
              </div>
              <div className="col-12 pt-2 pb-1 ">{product.name}</div>
              <Rating
                className="col-12 "
                emptySymbol="fa fa-star-o fs-0-8"
                fullSymbol="fa fa-star fs-0-8"
                initialRating={parseFloat(product.rating)}
                readonly
               />
              <div className="col-8 pt-2 pb-2 font-weight-bold text-left">
                {product.price}
              </div>
              <Query query={GET_WISH_SHOPLIST}>
                {({ data, client }) => {
                  // if(!data.shopList){
                  //   client.writeData({ data:  {shopList:[],cost:0}  });
                  // }
                  //console.log(data);
            return  <div className="mini-cart col-4 pt-2 pb-2 font-weight-bold text-right">
                <i  className= {"  fa   fs-1-5 pointer   "+(data.shopList && data.shopList.findIndex(x=>x._id===product._id) != -1 ? " fa-cart-arrow-down text-coral":" fa-shopping-cart " )}
                  onClick={e =>{
                    e.preventDefault();
                    //client.writeData({data: {shopList: {_id:"x2sddoijkdl",qty:"2",__typename:"Item_qty"} }})
                    this.props.handleShop(client,data,product._id,product.price)
                    }}
                     ></i>
              </div>
              }}
            </Query>

            </div>
            </Link>
            {/* { (this.props.shopList.indexOf(product._id)=== -1)?
              <button
              data-id={product._id}
              className= " col-12 btn  add-to-cart  pl-0 pr-0"
              onClick={event => this.props.handleShop(event.target.dataset.id, product.price)}>
              <div className="row m-0 d-flex align-items-center">
                <div className="col-8">Ajouter au panier</div>
                <div className="col-4">
                  <i  className=  "col-12  pt-0 pb-0 fa  fa-2x pointer  fa-shopping-cart "   ></i>
                </div>
              </div>
            </button>
            :
            <button
              data-id={product._id}
              className= " col-12 btn  btn-outline-success rounded-0 pl-0 pr-0"
              onClick={event => this.props.handleShop(event.target.dataset.id, product.price)}>
              <div className="row m-0 d-flex align-items-center">
                <div className="col-8">Dans le panier</div>
                <div className="col-4">
                  <i  className=  "col-12  pt-0 pb-0 fa  fa-2x pointer  fa-check"   ></i>
                </div>
              </div>
            </button>
            } */}
          </div>

        });
      }}
    </Query>
    </div>

  }

}
export default injectIntl(PopProducts);
