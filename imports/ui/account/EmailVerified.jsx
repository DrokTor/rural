import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import  ReactLoading  from 'react-loading';
import queryString from 'query-string';
import { injectIntl, defineMessages } from "react-intl";

const messages = defineMessages({
  content1: {
    id: 'emailVerified.content1',
    defaultMessage: 'Adresse confirmé !'
  },
  content2: {
    id: 'emailVerified.content2',
    defaultMessage: 'Erreur lors de la confirmation !'
  },

})

class EmailVerified extends Component {

  constructor(props){
    super(props);

    this.state={
      search : queryString.parse(location.search),
      loading : true,
      error  : '',

    }

   }

  componentDidMount() {
    Accounts.verifyEmail(this.state.search.t,err=>{
      err ?
        this.setState({success : false, error : err, loading : false })
        :
        this.setState({success: true, loading : false});

    })
  }


  render() {

    const {intl:{formatMessage}} = this.props;


    if(this.state.loading){
      return   <ReactLoading className="mx-auto mt-5" type={'spin'} color={"#0093ff"}  height={50} width={50} />
    }

    if(this.state.success){
      setTimeout(()=>{
        console.log('time');
        this.props.history.push('/user/profile')
      },5000)
      return <div className="alert alert-info col-4 p-4 mx-auto mt-5 text-center">
                <p className=" col-12 "> {formatMessage(messages.content1)}</p>
                {/* <p className="col-12">Cliquer sur le lien qui vous a été envoyé pour confirmer votre adresse mail et terminer l'enregistrement.</p> */}

              </div>
    }else {
      return <div className="alert alert-danger col-4 p-4 mx-auto mt-5 text-center">
                <p className=" col-12 "> {formatMessage(messages.content2)}</p>
              </div>
    }
  }


}

export default injectIntl(EmailVerified)
