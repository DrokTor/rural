import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import ReactLoading from 'react-loading';
import { injectIntl, defineMessages } from "react-intl";
import Client from '../../api/apolloClient';
import gql from "graphql-tag";

const messages = defineMessages({
  content1: {
    id: 'login.content1',
    defaultMessage: 'vous êtes déja connecté !'
  },
  content2: {
    id: 'login.content2',
    defaultMessage: 'Email'
  },
  content3: {
    id: 'login.content3',
    defaultMessage: 'Mot de passe'
  },
  content4: {
    id: 'login.content4',
    defaultMessage: 'créer un compte'
  },
  content5: {
    id: 'login.content5',
    defaultMessage: 'mot de passe oublié ?'
  },
  content6: {
    id: 'login.content6',
    defaultMessage: 'se connecter'
  },
  content7: {
    id: 'login.content7',
    defaultMessage: 'votre adresse e-mail n\'a pas été vérifiée, veuillez cliquer sur le lien qui vous a été envoyé.'
  },
})

const CHECK_EMAIL=gql`
  query CheckEmail($email: String!){
    checkEmail(email: $email)
  }
`
class Login extends Component {

  constructor(props){
    super(props);

    this.state={
      email : '',
      password : '',
      error  : '',
    }

    this.login = this.login.bind(this);
  }

  async login(){

    const {intl:{formatMessage}} = this.props;
    const {data :{checkEmail}} = await Client.query({query:CHECK_EMAIL,fetchPolicy:'network-only',variables:{email:this.state.email}});
    //console.log(data);
    //console.log(checkEmail);
    if(checkEmail === false){
      this.setState({error : formatMessage(messages.content7)});
    }else{

      this.setState({error : ''});
      Meteor.loginWithPassword(this.state.email, this.state.password, (err)=>{
        if(err)
        this.setState({error : err.reason});
        else this.props.history.push('/');
        //else this.props.history.goBack();
      })
    }
    //alert(this.state.email)
  }
  render() {
    const {intl:{formatMessage}} = this.props;


    return (
      <div className="min-height-500">
      {
        Meteor.userId() && <div className="col-5 mx-auto alert alert-primary text-center">{formatMessage(messages.content1)}</div>
      }
      {!Meteor.userId() && <div className="col-6 p-4 mx-auto border rounded border-light bg-light mt-5">

          <form className="row pt-3 pb-3">
            <div  className="row col-12">
              {this.state.error && <p className="text-danger col-8 mx-auto ">{this.state.error}</p>}
            </div>
            <div  className="row col-12">
            <label className="col-8 mx-auto " >{formatMessage(messages.content2)}</label>
            <input
              type = "email"
              className="col-8 mx-auto mb-4 p-2"
              onChange={(event)=>this.setState({email: event.target.value})}
            />
            <label className="col-8 mx-auto " >{formatMessage(messages.content3)}</label>
            <input
              type = "password"
              className="col-8 mx-auto mb-5 p-2"
              onChange={(event)=>this.setState({password: event.target.value})}
            />
            </div>
            <div  className="row col-12">
              <div className="col-3 p-0 offset-2">
                <Link to="/user/new" className=" p-0 col-12 d-block" >{formatMessage(messages.content4)}</Link>
                <Link to="/user/forgot" className=" p-0 col-12 d-block" >{formatMessage(messages.content5)}</Link>
              </div>
              <input
                type="button"
                value={formatMessage(messages.content6)}
                className="btn btn-primary col-3 p-3 offset-2"
                onClick={ this.login }
              />
            </div>
          </form>
        </div>
      }


    </div>
    );
  }


}

export default injectIntl(Login);
