import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import ReactLoading from 'react-loading';





export default class PasswordForgotten extends Component {

  constructor(props){
    super(props);

    this.state={
      email : '',
      sending : false,
      success : false,

    }

    this.forgot = this.forgot.bind(this);
  }

  forgot(){

    this.setState({error : '',sending : true});
    Accounts.forgotPassword({email : this.state.email}, (err)=>{
      if(err)
      this.setState({error : err.reason,sending : false});
      else{
        this.setState({success: true,sending : false});
      }
      //this.setState({sending : false});
    })
    //alert(this.state.email)
  }

  render() {


    return (
      <React.Fragment>
      {
        this.state.success && <div className="col-6 p-4 mx-auto mt-5 alert alert-primary text-center">Cliquer sur le lien qui vous a été envoyé pour réinitialiser votre mot de passe.</div>
      }
      {

  !this.state.success && <div className="col-6 p-4 mx-auto border rounded border-light bg-light mt-5">
        <div>
          <form className="row pt-3 pb-3">
            <div  className="row col-12">
              {this.state.error && <p className="text-danger col-8 mx-auto ">{this.state.error}</p>}
            </div>
            <div  className="row col-12">
            <label className="col-8 mx-auto " >Email</label>
            <input
              type = "email"
              className="col-8 mx-auto mb-4 p-2"
              onChange={(event)=>this.setState({email: event.target.value})}
            />
            <div className="row col-12 mx-auto p-0">
            <div className="col-1 offset-2">{this.state.sending && <ReactLoading type={'spin'} color={"#0093ff"}  height={50} width={50} />}</div>
            <input
              type="button"
              value="envoyer"
              className="btn btn-primary col-3 p-3 offset-4"
              onClick={ this.forgot }
            />
            </div>
            </div>
          </form>

        </div>

      </div>
      }
      </React.Fragment>
    );
  }


}
