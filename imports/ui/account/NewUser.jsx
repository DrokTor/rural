import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import  ReactLoading  from 'react-loading';
import { injectIntl, defineMessages } from "react-intl";

const messages = defineMessages({
  content0: {
    id: 'newUser.content0',
    defaultMessage: 'Enregistrement réussi !'
  },
  content1: {
    id: 'newUser.content1',
    defaultMessage: 'Cliquer sur le lien qui vous a été envoyé pour confirmer votre adresse mail et terminer l\'enregistrement.'
  },
  content2: {
    id: 'login.content2',
    defaultMessage: 'Email'
  },
  content3: {
    id: 'login.content3',
    defaultMessage: 'Mot de passe'
  },
  content4: {
    id: 'newUser.content3',
    defaultMessage: 'Mot de passe (vérification)'
  },
  content5: {
    id: 'newUser.content4',
    defaultMessage: 'créer'
  },




})

class NewUser extends Component {

  constructor(props){
    super(props);

    this.state={
      email : '',
      password : '',
      passwordCheck : '',
      error  : '',
      sending: false,
    }

    this.createUser = this.createUser.bind(this);
  }

  createUser(){

    this.setState({error : '',sending: true});
    Meteor.call('users.create',{email:this.state.email, password: this.state.password}, (error)=>{
      if(error)
      this.setState({error : error.reason, sending: false});
      else {
        //this.props.history.push('/');
        this.setState({registered: true});
      }
      //this.setState({sending: false});
    })
    //alert(this.state.email)
  }

  render() {

    const {intl:{formatMessage}} = this.props;

    return (
      <div className="min-height-500">
      {
        this.state.registered ?
      <div className="alert alert-info col-6 p-4 mx-auto mt-5 text-center">
        <p className="font-weight-bold col-12 ">{formatMessage(messages.content0)} </p>
        <p className="col-12">{formatMessage(messages.content1)}</p>
      </div>
      :
      <div className="col-6 p-4 mx-auto border rounded border-light bg-light mt-5">
        <div>
          <form className="row pt-3 pb-3">
            <div  className="row col-12">
              {this.state.error && <p className="text-danger col-8 mx-auto ">{this.state.error}</p>}
            </div>
            <div  className="row col-12">
            <label className="col-8 mx-auto " >{formatMessage(messages.content2)}</label>
            <input
              type = "email"
              className="col-8 mx-auto mb-4 p-2"
              onChange={(event)=>this.setState({email: event.target.value})}
            />
            <label className="col-8 mx-auto " >{formatMessage(messages.content3)}</label>
            <input
              type = "password"
              className="col-8 mx-auto mb-5 p-2"
              onChange={(event)=>this.setState({password: event.target.value})}
            />
            <label className="col-8 mx-auto " >{formatMessage(messages.content4)}</label>
            <input
              type = "password"
              className="col-8 mx-auto mb-5 p-2"
              onChange={(event)=>this.setState({passwordCheck: event.target.value})}
            />
            </div>
            <div  className="row col-12">
              {/*<Link to="/user/new" className=" p-0 col-3  offset-2" >créer un compte</Link>*/}
              {/*<div className="row col-12 mx-auto p-0">*/}
              <div className="col-1 offset-2">
                {this.state.sending && <ReactLoading type={'spin'} color={"#0093ff"}  height={50} width={50} />}
              </div>
              <input
                type="button"
                value={formatMessage(messages.content5)}
                className="btn btn-primary col-3 p-3 offset-4"
                onClick={ this.createUser }
              />
            </div>
          </form>
        </div>

      </div>
      }
    </div>
    );
  }


}

export default injectIntl(NewUser);
