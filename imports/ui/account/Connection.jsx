import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Query } from 'react-apollo';
import gql from "graphql-tag";
import {Link} from 'react-router-dom';
import { withTracker } from 'meteor/react-meteor-data';
import { injectIntl, defineMessages } from "react-intl";
import Client from '../../api/apolloClient';

const messages = defineMessages({
  content1: {
    id: 'connection.content1',
    defaultMessage: 'Ma boutique'
  },
  content2: {
    id: 'connection.content2',
    defaultMessage: 'Mes achats'
  },
  content3: {
    id: 'connection.content3',
    defaultMessage: 'Se déconnecter'
  },
  content4: {
    id: 'connection.content4',
    defaultMessage: 'Se connecter'
  },
  content5: {
    id: 'connection.content5',
    defaultMessage: 'Mon profil'
  },
})

const GET_PROFILE=gql`
  query GetProfile{
    getProfile{
      firstName
      lastName
      birthDate
      country
      city
      address
      phone
    }
  }
`

// App component - represents the whole app

class Connection extends Component {
//{Meteor.user() ? <span>user</span>:

  constructor(props){
      super(props);

      this.state={
        profileWarning: false,
      }
  }

  async componentDidMount() {
    const {data } = await Client.query({query:GET_PROFILE,fetchPolicy:'network-only'});
     const profile = await data.getProfile ;
    // console.log(data.getProducts[0]);
       //console.log(profile);
       if(!profile){
         this.setState({profileWarning:true});
       }
  }

  render() {

    const {intl:{formatMessage}} = this.props;

    return (
      <Query
        query={gql`
          {
            getUser{

              email
            }
          }
        `}
        //pollInterval={300}
        >
        {({ loading, error, data, context }) => {

          //console.log(Meteor.userId());
          if (loading) return <p>Loading...</p>;
          //console.log(data.getUser);
          //const user = Meteor.user();
        return  this.props.user ?//(   data.getUser && data.getUser.email != null ) ?
        <ul className="navbar-nav col-3 ">
          <li className="nav-item dropdown">
            <span className="nav-link pointer dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <a href="#">{this.props.user.emails[0].address} {this.state.profileWarning? <i className="fa  fa-exclamation-triangle"></i>:''}</a> {/*  data.getUser */}
            </span>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
              <Link to="/store/dashboard" className="dropdown-item">{formatMessage(messages.content1)}  </Link>
              <Link to="/purchases" className="dropdown-item">{formatMessage(messages.content2)}  </Link>
              <Link to="/user/profile" className="dropdown-item">{formatMessage(messages.content5)}  {this.state.profileWarning? <i className="fa  fa-exclamation-triangle"></i>:''}</Link>
              <a onClick={()=>{Meteor.logout( err => this.props.history.push('/login') )}} className="dropdown-item" href="#">{formatMessage(messages.content3)}</a>
            </div>
          </li>
        </ul>
         :
          <Link to="/login">
            <button className="btn btn-outline-brown" >
              <i className="fa fa-user"></i> {formatMessage(messages.content4)}
            </button>
          </Link>


        }}
      </Query>
    );

  }

}

//export injectIntl(Connection)

export default withTracker(()=>{

//console.log(Meteor.userId());

return {
user : Meteor.user(),
}

})(injectIntl(Connection))
