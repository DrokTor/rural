import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import queryString from 'query-string';



Accounts.onResetPasswordLink((token,done)=>{
alert('ff');

})

export default class PasswordReset extends Component {

  constructor(props){
    super(props);

    this.state={
      password : '',
      passwordCheck : '',
      search : queryString.parse(location.search),
      success : false,
    }


    this.reset = this.reset.bind(this);

  }



  reset(){

    this.setState({error : ''});
    Accounts.resetPassword(this.state.search.t,this.state.password, (err)=>{
      if(err)
      this.setState({error : err.reason});
      else this.setState({success : true});
    })
    //alert(this.state.email)
  }
  render() {


    return (
      <React.Fragment>
      {
        this.state.success && <div className="col-6 p-4 mx-auto mt-5 alert alert-primary text-center">Mot de passe réinitialisé avec succès!</div>
      }
      { !this.state.success && <div className="col-6 p-4 mx-auto border rounded border-light bg-light mt-5">
        <div>

          <form className="row pt-3 pb-3">
            <div  className="row col-12">
              {this.state.error && <p className="text-danger col-8 mx-auto ">{this.state.error}</p>}
            </div>
            <div  className="row col-12">
            <label className="col-8 mx-auto " >New password</label>
            <input
              type = "password"
              className="col-8 mx-auto mb-3 p-2"
              onChange={(event)=>this.setState({password: event.target.value})}
            />
            <label className="col-8 mx-auto " >New password (verification)</label>
            <input
              type = "password"
              className="col-8 mx-auto mb-4 p-2"
              onChange={(event)=>this.setState({passwordCheck: event.target.value})}
            />
            <input
              type="button"
              value="envoyer"
              className="btn btn-primary col-3 p-3 offset-7"
              onClick={ this.reset }
            />
            </div>
          </form>

        </div>

      </div>}
      </React.Fragment>
    );
  }


}
