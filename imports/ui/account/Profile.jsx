import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import ReactLoading from 'react-loading';
import { injectIntl, defineMessages } from "react-intl";
import Client from '../../api/apolloClient';
import gql from "graphql-tag";
import { Query,Mutation } from 'react-apollo';
import {Motion, spring, presets} from 'react-motion';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/style.css';

const messages = defineMessages({
  firstName: {
    id: 'profile.firstName',
    defaultMessage: 'Prénom'
  },
  lastName: {
    id: 'profile.lastName',
    defaultMessage: 'Nom'
  },
  country: {
    id: 'profile.country',
    defaultMessage: 'Pays'
  },
  city: {
    id: 'profile.city',
    defaultMessage: 'Ville'
  },
  address: {
    id: 'profile.address',
    defaultMessage: 'Adresse'
  },
  phone: {
    id: 'profile.phone',
    defaultMessage: 'Téléphone'
  },
  save: {
    id: 'general.save',
    defaultMessage: 'Enregistrer'
  },
  birthDate: {
    id: 'profile.birthDate',
    defaultMessage: 'Date de naissance'
  },
  fillAll: {
    id: 'general.fillAll',
    defaultMessage: 'Merci de remplir tous les champs.'
  },
})

const CHECK_EMAIL=gql`
  query CheckEmail($email: String!){
    checkEmail(email: $email)
  }
`
const GET_PROFILE=gql`
  query GetProfile{
    getProfile{
      firstName
      lastName
      birthDate
      country
      city
      address
      phone
    }
  }
`

const SAVE_PROFILE=gql`
  mutation SaveProfile($firstName: String!,$lastName: String!,$birthDate: Date!,$country: String!,$city: String!,$address: String!,$phone: String!){
    saveProfile(firstName: $firstName,lastName: $lastName,birthDate: $birthDate,country: $country,city: $city,address: $address,phone: $phone){

      lastName

    }
  }
`
class Profile extends Component {

  constructor(props){
    super(props);

    this.state={
      firstName : '',
      lastName : '',
      birthDate: '',
      country : '',
      city : '',
      address : '',
      phone : '',
      open:false,
      error  : '',
    }

    this.login = this.login.bind(this);
    this.check = this.check.bind(this);

  }


  async componentDidMount() {
    const {data } = await Client.query({query:GET_PROFILE,fetchPolicy:'network-only'});
     const profile = await data.getProfile ;
    // console.log(data.getProducts[0]);
      //console.log(profile);
    this.setState({
      firstName : profile.firstName,
      lastName : profile.lastName,
      birthDate: profile.birthDate,
      country : profile.country,
      city : profile.city,
      address : profile.address,
      phone : profile.phone,

    })
  }

  async login(){

    const {intl:{formatMessage}} = this.props;
    const {data :{checkEmail}} = await Client.query({query:CHECK_EMAIL,fetchPolicy:'network-only',variables:{email:this.state.email}});
    //console.log(data);
    //console.log(checkEmail);
    if(checkEmail === false){
      this.setState({error : formatMessage(messages.fillAll)});
    }else{

      this.setState({error : ''});
      Meteor.loginWithPassword(this.state.email, this.state.password, (err)=>{
        if(err)
        this.setState({error : err.reason});
        else this.props.history.push('/');
        //else this.props.history.goBack();
      })
    }
    //alert(this.state.email)
  }

  check(){
    const {intl:{formatMessage}} = this.props;
    const {firstName,
          lastName,
          birthDate,
          country,
          city,
          address,
          phone} = {... this.state}

      // console.log(firstName);
      // console.log(lastName);
      // console.log(birthDate);
      if(firstName.trim() && lastName.trim() && birthDate)
      {
        return true;
      }else {
        this.setState({error: formatMessage(messages.fillAll)});
        return false;
      }
  }

  render() {
    const {intl:{formatMessage}} = this.props;


    return (
      <div className="min-height-500">
      {
        // Meteor.userId() && <div className="col-5 mx-auto alert alert-primary text-center">{formatMessage(messages.firstName)}</div>
      }
      {//!Meteor.userId() &&
        <div className="col-6 p-4 mx-auto border rounded  overflow-hidden-x   mt-5">

          <form className="row mx-auto pt-3 pb-3 col-12 ">
            <div  className="row col-12">
              {this.state.error && <p className="text-danger col-8 mx-auto ">{this.state.error}</p>}
            </div>
            <Motion  style={{x: spring(this.state.open ? -50 : 0, presets.stiff)}}>
            {({x}) =>
            <div  className="row width-double" style={{
                WebkitTransform: `translate3d(${x}%, 0, 0)`,
                transform: `translate3d(${x}%, 0, 0)`,
            }}>
              <div className="row m-0 col-6  ">
            <label className="col-8 mx-auto " >{formatMessage(messages.firstName)}</label>
            <input
              type = "text"
              required
              value={this.state.firstName}
              className="col-8 mx-auto mb-4 p-2"
              onChange={(event)=>this.setState({firstName: event.target.value})}
            />
            <label className="col-8 mx-auto " >{formatMessage(messages.lastName)}</label>
            <input
              type = "text"
              required
              value={this.state.lastName}
              className="col-8 mx-auto mb-4 p-2"
              onChange={(event)=>this.setState({lastName: event.target.value})}
            />
            <label className="col-8 mx-auto " >{formatMessage(messages.birthDate)}</label>
            {/* <input
              type = "text"
              required
              className="col-8 mx-auto mb-4 p-2"
              onChange={(event)=>this.setState({birthDate: event.target.value})}
            /> */}
            <div className="col-8 mx-auto mb-4 p-0">
              <DatePicker
                className=" col-12 p-2"
                selected={this.state.birthDate}
                onChange={birthDate=>this.setState({birthDate})}
                showMonthDropdown
                showYearDropdown
                scrollableYearDropdown
                yearDropdownItemNumber={35}
              />
            </div>
              </div>
            <div className="row m-0 col-6  ">
              <div className="col-4 offset-2 pl-0">
              <label className="col-12 mx-auto " >{formatMessage(messages.country)}</label>
              <input
                type = "text"
                required
                value={this.state.country}
                className="col-12 mx-auto mb-4 p-2"
                onChange={(event)=>this.setState({country: event.target.value})}
              />
            </div>
            <div className="col-4 pr-0">
            <label className="col-12 mx-auto " >{formatMessage(messages.city)}</label>
            <input
              type = "text"
              required
              value={this.state.city}
              className="col-12 mx-auto mb-4 p-2"
              onChange={(event)=>this.setState({city: event.target.value})}
            />
            </div>
            <label className="col-8 mx-auto " >{formatMessage(messages.address)}</label>
            <input
              type = "text"
              required
              value={this.state.address}
              className="col-8 mx-auto mb-4 p-2"
              onChange={(event)=>this.setState({address: event.target.value})}
            />
            <label className="col-8 mx-auto " >{formatMessage(messages.phone)}</label>

            <PhoneInput
              ///placeholder="Enter phone number"
              value={ this.state.phone }
              className="col-8 mx-auto mb-4 p-2"
              country="DZ"
              onChange={ phone => this.setState({ phone }) }/>

              </div>
            </div>
            }
            </Motion>
            <div className="row col-7 mx-auto p-0">
              {!this.state.open && <div className="col-3  ">
                <i className="pointer fa fa-2x text-primary fa-angle-double-right"
                  onClick={()=>{
                    this.check()?
                    this.setState({open: !this.state.open})
                    :
                    ''
                  }}></i>
              </div>}
              {this.state.open && <div className="col-3 ">
                <i className="pointer fa fa-2x text-primary fa-angle-double-left" onClick={()=>this.setState({open: !this.state.open})}></i>
              </div>}
              <div className="col-3 ">
              {  <Motion style={{x: spring(this.state.saved ? 1 : 0)}}>
                {({x}) =>
                <i className="  fa fa-2x text-success fa-check"
                  style={{
                    WebkitTransition: `opacity .03s`,
                    transition: `opacity  .03s`,
                    opacity: `${x}`,
                  }}></i> }
                </Motion> }
              </div>
              { <div className="col-6 ">
                <Motion style={{x: spring(this.state.open ? 1 : 0)}}>
                  {({x}) =>
                  <Mutation
                    mutation={SAVE_PROFILE}
                    onCompleted={(data=>{
                      this.setState({saved:true})
                      setTimeout(()=>{this.setState({saved:false})}, 2000);
                    })}
                    >
                    {(saveProfile,{data,loading,error})=>{

                  return <button className="btn btn-primary col-12" style={{
                            WebkitTransition: `opacity .103s`,
                            transition: `opacity  .103s`,
                            opacity: `${x}`,
                            display: x === 0 ? "none":"block",

                        }}
                          //{ x === 0 ? disabled:""}
                          //disabled='false'
                          onClick={e=>{
                            e.preventDefault();
                            const { open,error,saved,...profile }= this.state;
                            console.log(profile);
                            saveProfile({variables:profile});
                          }}
                        >{formatMessage(messages.save)}</button>
                  }}
                </Mutation>
                  }
                </Motion>
              </div>}
            </div>
          </form>
        </div>
      }


    </div>
    );
  }


}

export default injectIntl(Profile);
