import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { Query } from "react-apollo";
import gql from "graphql-tag";


import Connection from './account/Connection.jsx';


// App component - represents the whole app

export default class Footer extends Component {

  render() {
    const GET_WISH_SHOPLIST = gql`
    {
      wishList @client
      shopList @client {
        _id
        qty
      }
    }
    `;

    return (

      <div className="row   footer-container mt-5">
        <nav className="main-footer navbar navbar-expand-lg col-12  ">

          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="float-right collapse navbar-collapse" id="navbarSupportedContent">
            {/* <ul className="navbar-nav offset-8 ">
              <li className="nav-item dropdown">
                <span className="nav-link pointer dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Modules
                </span>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a onClick={()=>{this.props.onClick('deviations')}} className="dropdown-item" href="#">Deviation & CAPA</a>
                  <a onClick={()=>{this.props.onClick('administration')}} className="dropdown-item" href="#">Administration</a>
                </div>
              </li>
            </ul> */}
            <div className="float-right row col-7 offset-3 d-flex align-items-center">
              <div className="col-6 border-bottom-brown search">
                <input
                   type="text"
                   className="col-10 border-0"
                   placeholder="not working yet"
                  />
                <i className="col-2 pointer fa fa-search fs-1-5" onClick={()=>this.props.loadPage('consultation')}></i>
              </div>
              {/* <Query query={GET_WISH_SHOPLIST}
                //onCompleted={(data)=> this.props.initWishList(data.wishList)}
                fetchPolicy={"cache-first"}
                >
                {({ data, client }) => {

                return <React.Fragment><Link to="/wishlist" className="col-3 no-deco">
                  <i className={"col-12 pointer fa  fa-wish text-right "+(data.wishList && data.wishList.length>0 ? "fa-heart wish-color": "fa-heart-o")}  >
                    {data.wishList &&  data.wishList.length>0 && <span className="wish-count">{ data.wishList.length}</span>}
                  </i>
                </Link>
                <Link to="/shoplist" className="col-3 no-deco d-flex align-items-center">
                  <i className={"col-12 p-0 pointer fa  fa-2x  text-center "+(data.shopList && data.shopList.length>0 ? "fa-cart-arrow-down ": "fa-shopping-cart")}  >
                    {data.shopList &&  data.shopList.length>0 && <span className="shop-count">{data.shopList.length}</span>}
                  </i>
                </Link>
                </React.Fragment>
                }}
              </Query> */}
              {/* <Link to="/wishlist" className="col-4 no-deco">
                <i className={"col-12 pointer fa  fa-wish "+(this.props.wishList.length>0 ? "fa-cart-arrow-down  wish-color": "fa-shopping-cart")}  >
                  {this.props.wishList.length>0 && <span className="wish-count">{ this.props.wishList.length}</span>}
                </i>
              </Link> */}


            </div>
            {/*<Logout />*/}
          </div>
        </nav>
      </div>
    );

  }

}
