import React, { Component } from 'react';
import { Query } from "react-apollo";
import gql from "graphql-tag";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
//import Icon from '@material-ui/core/Icon';
import {Link} from "react-router-dom";
//import {DeleteIcon} from '@material-ui/icons/DeleteIcon';
import {Delete , ShoppingCart, AddShoppingCart, RemoveShoppingCart} from '@material-ui/icons';
import { injectIntl, defineMessages } from "react-intl";
import Rating from "react-rating";

const messages = defineMessages({
  title: {
    id: 'TopReviews.title',
    defaultMessage: 'Les meilleurs avis'
  }
})

class TopReviews extends Component {

  render(){
    const {intl:{formatMessage}} = this.props;

    const GET_RATINGS = gql`
      query getRatings( $limit: Int = 4 ){
        getRatings( limit: $limit)  {
          _id
          product_id
          rating
          title
          comment
        }
      }
    `;

    return <div className="row col-12 px-5 pt-1 pb-5 mx-auto">
      <h5 className="row col-12 mb-4">{formatMessage(messages.title)}</h5>
      <Query
        query={GET_RATINGS}
        fetchPolicy={"cache-and-network"}
        >
        {({ loading, error, data })=>{
          if(loading) return <div>loading...</div>;
          if(error) return <div>{error}</div>;

          //console.log(data.getRatings);
          if(data.getRatings )
          return  data.getRatings.map((rating,index)=>{
            //console.log(rating);
          return <div key={index} className="col-3 ">
                  <div className="col-12 p-3 border border-bottom-brown-2">
                    <Link className="no-deco" to={"/product/"+rating.product_id+"#"+rating._id} >
                    <Rating
                      className="pb-2 col-12 p-0"
                      initialRating={rating.rating}
                      readonly
                      emptySymbol="fa fa-star-o "
                      fullSymbol="fa fa-star "
                    />
                    <div className="col-12 p-0 pb-2 font-weight-bold">{rating.title}</div>
                    <div className="">{rating.comment.substr(0, 60)+" .." }</div>
                    </Link>
                  </div>
                </div>
              })
          return <div>loading...</div>;
        }}
      </Query>
    </div>
  }
}
export default injectIntl(TopReviews);
