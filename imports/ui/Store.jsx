import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Query } from 'react-apollo';
import gql from "graphql-tag";
import { Link,BrowserRouter,Route } from 'react-router-dom'
import { Button, Col,MDBDatePicker, Container } from 'mdbreact';
import { injectIntl, defineMessages } from "react-intl";

import Upload from './store/Upload.jsx';
import NewProduct from './store/NewProduct.jsx';
import ListProducts from './store/ListProducts.jsx';
import Orders from './store/Orders.jsx';
import Dashboard from './store/Dashboard.jsx';

const messages = defineMessages({
  orders: {
    id: 'store.orders',
    defaultMessage: 'Commandes'
  },
  newProduct: {
    id: 'store.newProduct',
    defaultMessage: 'Ajouter un produit'
  },
  listProducts: {
    id: 'store.listProducts',
    defaultMessage: 'Liste des produits'
  },
  dashboard: {
    id: 'store.dashboard',
    defaultMessage: 'Tableau de bord'
  },
})





class Store extends Component {
//{Meteor.user() ? <span>user</span>:



 render() {
   const {intl:{formatMessage}} = this.props;

   return (
     <div className="row   h-100">
       <div className="col-2 col-xs-12 navbar-dark bg-light  ">
         <ul className=" text-dark list-unstyled pt-5">
           <Link className="no-deco" to="/store/orders">
            <li className="mb-5 col-12 text-center pointer store-link">
            <i className="fa fa-shopping-cart fa-2x col-12"></i>
            <div>{formatMessage(messages.orders)}</div>
            </li>
          </Link>
           <Link className="no-deco" to="/store/products/new">
            <li className="mb-5 col-12 text-center pointer store-link">
            <i className="fa fa-cube fa-2x col-12"></i>
            <div>{formatMessage(messages.newProduct)}</div>
            </li>
          </Link>
           <Link className="no-deco" to="/store/products/list">
            <li className="mb-5 col-12 text-center pointer store-link">
            <i className="fa fa-th-list fa-2x col-12"></i>
            <div>{formatMessage(messages.listProducts)}</div>
            </li>
          </Link>
           <Link className="no-deco" to="/store/dashboard">
            <li className="mb-5 col-12 text-center pointer store-link">
            <i className="fa fa-tachometer fa-2x col-12"></i>
            <div>{formatMessage(messages.dashboard)}</div>
            </li>
          </Link>
         </ul>
       </div>
       <div className="col-10 pt-3 overflow">
         <div className="row col-11 mx-auto ">
            <Route exact path="/store/products/new" component={NewProduct}/>
            <Route exact path="/store/products/edit/:id"
            component={props =>  <NewProduct {...props}  edit={true}   /> }/>
            <Route exact path="/store/products/list" component={ListProducts}/>
            <Route exact path="/store/orders" component={Orders}/>
            <Route exact path="/store/dashboard" component={Dashboard}/>
         </div>
       </div>
     </div>
   );
 }

}
export default injectIntl(Store);
