import React, { Component } from 'react';
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import { withApollo } from "react-apollo";

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
//import Icon from '@material-ui/core/Icon';
import {Link} from "react-router-dom";
//import {DeleteIcon} from '@material-ui/icons/DeleteIcon';
import {Delete , ShoppingCart, AddShoppingCart, RemoveShoppingCart} from '@material-ui/icons';
import CircularProgress from '@material-ui/core/CircularProgress';
import { _ }  from "lodash";
import { injectIntl, defineMessages } from "react-intl";

const messages = defineMessages({
  shopListEmpty: {
    id: 'shopList.shopListEmpty',
    defaultMessage: 'Vous n\'avez aucun produit dans votre panier.'
  },
  purchased: {
    id: 'shopList.purchased',
    defaultMessage: 'Votre commande a été envoyée avec succès.'
  },
  purchases: {
    id: 'shopList.purchases',
    defaultMessage: 'Voir mes commandes.'
  },
  quantity: {
    id: 'orders.content2',
    defaultMessage: 'Quantité'
  },
})

const NEW_ORDER = gql`
  mutation NewOrder($shopList : [ShopList!]!) {
    newOrder(shopList : $shopList)
  }
`;

const GET_PRODUCTS = gql`
  query getProducts($id: [ID]){
    getProducts(_id: $id)  {
      _id
      name
      category
      family
      price
      filenames
    }
  }
`;
const GET_WISH_SHOPLIST = gql`
{
  wishList @client
  shopList @client{
    _id
    qty
  }
  cost @client
}
`;

class ShopList extends Component {

  constructor(props){
    super(props);

    this.state={
      validated: false,
      products:[],
    }
    //
     //this.handleRemove = this.handleRemove.bind(this);

      //console.log(this.props.client);
  }

  // componentDidMount() {
  //   const list = this.props.client.readQuery({query:GET_WISH_SHOPLIST});
  //   console.log(list.shopList);
  //   //this.setState({products:list.shopList});
  //   // list.shopList.map((elem,index)=>{
  //   //   this.state.products.push({_id:elem,qty:1})
  //   // })
  // }



  render(){
    const {intl:{formatMessage}} = this.props;

    let variables = {};
    if( this.props.showShopList &&  this.props.showShopList.length>0){
      //console.log(_.map(this.props.showShopList,'_id'));
      variables.id  = _.map(this.props.showShopList,'_id') ;
    }
    //console.log(this.props.validated);
    if (this.props.validated){
      return <div className="min-height-500"><div className="alert alert-success mx-auto mt-5 col-6">
        {formatMessage(messages.purchased)} <Link to="/purchases">{formatMessage(messages.purchases)}</Link>
      </div></div>
    }
    //console.log(this.props.showShopList &&  this.props.showShopList.length===0);
    if( this.props.showShopList &&  this.props.showShopList.length===0){
      return <div className="min-height-500"><div className="col-4 mx-auto alert-warning mt-5 p-5 text-center">{formatMessage(messages.shopListEmpty)}</div></div>
    }
    return <div className=" min-height-500 col-8 mx-auto mt-5  ">
      {
        this.props.showShopList &&
        <Mutation
             mutation={NEW_ORDER}
             onCompleted={ (data,error) => this.props.handleOrder() }
             // update={(cache, { data: { newOrder } }) => {
             //    //console.log(newOrder);
             //    if(cache.data.data.ROOT_QUERY.getProducts){
             //         //console.log(cache.data.data.ROOT_QUERY);
             //         const products  = cache.readQuery({ query: GET_PRODUCTS });
             //         //console.log(products);
             //         cache.writeQuery({
             //           query: GET_PRODUCTS,
             //           data: { getProducts: products.getProducts.concat([newOrder]) }
             //         });
             //    }
             //  }}
             >

           {(newOrder, { loading, error,data }) => {
       if (loading){
         //console.log('data');
         return <div className="mx-auto mt-5 col-3"><CircularProgress   /></div>;
       }

     return <div className="row col-12 mb-5 m-0">
             <Query query={GET_WISH_SHOPLIST} fetchPolicy={'cash-only'}>
               {(dataList,client)=>{
                 console.log(_.map(dataList.data.shopList,(el)=>{return {_id:el._id,qty:el.qty} }));
              return  <React.Fragment>
                <div className="row col-9 pl-4 pt-3 pb-2 border mr-0">
                <div className="col-3">Total:</div>

                <label className="col-9">{dataList.data.cost+" € "}</label>
              </div>
              <button className="btn btn-warning col-3   pt-2 pb-3"
                 onClick={()=>{
                   if(!Meteor.user()) this.props.history.push('/login');
                   newOrder({variables : {shopList : _.map(dataList.data.shopList,(el)=>{return {_id:el._id,qty:el.qty} })/*this.props.shopList*/}})}}>COMMANDER <i className="fa fa-cart-arrow-down fa-2x text-white"></i></button>
              </React.Fragment>
              }}
            </Query>
            </div>
        }}
      </Mutation>
      }
     <Query
      query={GET_PRODUCTS}
      variables={variables}
      fetchPolicy={"network-only"}
      //variables={{id : ["CJ2gth3WxgyQx2DCv"]  }}
      //pollInterval={2000}
      >
      {({ loading, error, data, variables }) => {
        //  console.log(variables);
        // data && console.log(data);
        if (loading) return <p>Loading...</p>;
        if (error) return <p>Error :(</p>;
        //random image remove for product
        let url = Meteor.absoluteUrl();
        url = url.split('://');
        url[1] = url[1].split(':');
        const baseurl = url[1][0].replace(/\/$/, ""); // remove trailing slash;

        url  = url[0]+"://"+baseurl+':3080/';


        return data.getProducts.map((product,index) => {
          //let img= "/assets/images/"+Math.floor((Math.random() * 4) + 1)+".jpeg";
          //console.log(product);
          //!this.state.products[product._id] && this.setState({products:{[product._id]:1}});
    return <div key= {index} className="col-12 mb-1 ">
            <div className="row col-12 border d-flex align-items-center pl-0" >
              <div className="col-2 p-0">
                <Link to={"/product/"+product._id} className="no-deco">
                  <img className="col-12 image-fluid p-0" src={url+product._id+"/"+product.filenames[0]}/>
                </Link>
              </div>
              <div className="col-3 pt-2 pb-2 ">{product.name}</div>
              <div className="row col-3 pt-2 pb-2 d-flex align-items-center">
                <div className="col-8">{formatMessage(messages.quantity)}</div>
                <Query query={GET_WISH_SHOPLIST}>
                  {
                    ({ data, client }) => {


            return    <input
                  type="number"
                  data-qty={1}
                  className="col-4 p-0"
                  min={1}
                  //value={  this.state.products.find(x=>x._id===product._id).qty   }
                  defaultValue={ data.shopList.find(x=>x._id===product._id).qty   }
                  //defaultValue={1}
                  onChange={
                     e=>{
                     e.preventDefault();
                     let products = data.shopList;
                     const indx = products.findIndex(x => x._id === product._id);
                     //console.log(indx);
                     let newCost=data.cost;
                     let oldProduct=products[indx];
                     newCost = parseFloat(data.cost)-parseFloat(oldProduct.qty*product.price)+parseFloat(e.target.value*product.price);
                     if(indx !== -1 ){
                       // if(products[indx].qty>e.target.value){
                       //   newCost = parseInt(data.cost)-parseInt(product.price);
                       // }else if(products[indx].qty<e.target.value) {
                       //   newCost = parseInt(data.cost)+parseInt(product.price);
                       // }
                       products[indx].qty = e.target.value;
                     }else{
                       products.push({_id:product._id, qty:e.target.value})
                     }
                     //products[product._id] = e.target.value;
                     //this.setState({products} )

                     //console.log(newCost);
                     //console.log(products);
                     client.writeData({data:{shopList:products,cost: newCost}});
                     //this.props.handleCost(newCost);
                      }
                    }
                />
                  }
                }
                </Query>
              </div>
              <div className="col-3 pt-2 pb-2 font-weight-bold text-right">
                {product.price}
              </div>
              <Query query={GET_WISH_SHOPLIST}>
                {({ data, client }) => {

            return  <div className="col-1 pt-2 pb-2 font-weight-bold text-right">
              <i data-id={product._id} className= " text-right fa   pointer fa-times text-secondary"
                  onClick={event => {
                    event.preventDefault();
                    let products = data.shopList;
                    const indx = products.findIndex(x => x._id === product._id);
                    let qty=1;
                    if(indx !== -1 ){
                      qty = products[indx].qty;
                    }
                    //console.log(qty);
                    //this.handleRemove(client,data,product._id,product.price);
                    this.props.handleShop(client,data,product._id,product.price,qty)
                  }}
                    ></i>
            </div>

              }}
            </Query>

              {/* <div  className="col-3 d-flex align-items-center">
                <div className="float-right">
                <i data-id={product._id} className={" fa   fa-wish pointer  "+(this.props.whishList.indexOf(product._id)=== -1 ? " fa-heart-o":" fa-heart wish-color" )} onClick={event => {event.preventDefault(); this.props.handleWish(event.target.dataset.id)}}></i>
                <i data-id={product._id} className= {" text-right fa  fa-2x pointer "+(this.props.shopList.indexOf(product._id)=== -1? " fa-shopping-cart ":" fa-cart-arrow-down cart-color")}  onClick={event => {event.preventDefault(); this.props.handleShop(event.target.dataset.id,product.price)}}></i>
                </div>
              </div> */}
            </div>

          </div>

        });
      }}
    </Query>
    </div>

  }

}

export default withApollo(injectIntl(ShopList));
