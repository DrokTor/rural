import React, { Component } from 'react';
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
//import Icon from '@material-ui/core/Icon';
import {Link} from "react-router-dom";
//import {DeleteIcon} from '@material-ui/icons/DeleteIcon';
import {Delete , ShoppingCart, AddShoppingCart, RemoveShoppingCart} from '@material-ui/icons';
import CircularProgress from '@material-ui/core/CircularProgress';
import 'typeface-raleway';
import { injectIntl, defineMessages } from "react-intl";
import arLocaleData from "react-intl/locale-data/en";
import esLocaleData from "react-intl/locale-data/de";


const messages = defineMessages({
  title: {
    id: 'home.title',
    defaultMessage: 'Vivez la passion des artisans'
  },
  content1_title: {
    id: 'home.content1.title',
    defaultMessage: 'Un produit avec une âme'
  },
  content1: {
    id: 'home.content1',
    defaultMessage: "Chaque jour nos artisans s'immerse dans leur passion pour donner vie à une merveille."
  },
  content2_title: {
    id: 'home.content2.title',
    defaultMessage: 'Une expérience unique'
  },
  content2: {
    id: 'home.content2',
    defaultMessage: "L'Algérie vous emmènne en voyage à travers des payasages à couper le souffle, avec une diversité inégalée."
  },
  content3_title: {
    id: 'home.content3.title',
    defaultMessage: 'Fait main'
  },
  content3: {
    id: 'home.content3',
    defaultMessage: "Les produits sont fait main avec passion et un grand dévouement."
  },
  content4_title: {
    id: 'home.content4.title',
    defaultMessage: 'Unique'
  },
  content4: {
    id: 'home.content4',
    defaultMessage: "Chaque produit reflète l'unicité de son créateur."
  },
  content5_title: {
    id: 'home.content5.title',
    defaultMessage: 'Sécurisé'
  },
  content5: {
    id: 'home.content5',
    defaultMessage: "Vos transactions sont sécurisées avec les dernières technologies."
  },
})

const NEW_ORDER = gql`
  mutation NewOrder($shopList : [ID!]!) {
    newOrder(shopList : $shopList)
  }
`;


class Home extends Component {

  constructor(props){
    super(props);

    this.state={
      validated: false,
    }
    //
    // this.handleWish = this.handleWish.bind(this);
  }

  componentDidMount () {
     window.onscroll =()=>{
      const newScrollHeight =  window.scrollY; //Math.ceil(window.scrollY / 50) *50 ;
      //console.log(newScrollHeight);
      if (this.state.currentScrollHeight != newScrollHeight){
          this.setState({currentScrollHeight: newScrollHeight})
      }
    }
  }



  render(){
    const {intl:{formatMessage}} = this.props;
    const opacity =  Math.min( (10 / this.state.currentScrollHeight)    , 1) || 1 ;
    const opacity2 =  Math.min( (80 / this.state.currentScrollHeight)    , 1) || 1 ;
    //console.log(opacity2);

    return <React.Fragment>
      <div style={{opacity: opacity2}}  className="row col-12 mx-auto mt-5 pt-5  ">
        <h3 style={{opacity}} className="slogan col-12 mx-auto mb-5   text-center">{formatMessage(messages.title)}</h3>
        <div className="col-7 row mx-auto">
          <div className="col-4 bg-beige m-0 p-4 position-absolute z-index-2">
            <label>
              {formatMessage(messages.content1_title)}
            </label>
            <p>
              {formatMessage(messages.content1)}
            </p>
          </div>
          <img style={{height: "78%"}} className="col-12   m-0 p-0" height="350" src="/assets/images/artisan2.jpeg" />
        </div>
        <div className="  col-4  mx-auto">
          <img className="col-12 p-0" src="/assets/images/algeria.jpeg" />
          <div className="algeria col-12 bg-beige p-4">
            <label>
              {formatMessage(messages.content2_title)}
            </label>
            <p>
              {formatMessage(messages.content2)}
            </p>
          </div>
        </div>
      </div>
        <div className="row col-11 p-0 mx-auto">
          <div className="col-4 ">
            <div className="row col-12   p-3">
              <i className="col-2 fa fa-check  fa-2x bg-beige d-flex align-items-center"></i>
              <div className="col-10"><label>
                {formatMessage(messages.content3_title)}
              </label>
              <p>
                {formatMessage(messages.content3)}
              </p></div>
            </div>
          </div>
          <div className="col-4 ">
            <div className="row col-12   p-3">
              <i className="col-2 fa fa-check  fa-2x bg-beige d-flex align-items-center"></i>
              <div className="col-10"><label>
                {formatMessage(messages.content4_title)}
              </label>
              <p>
                {formatMessage(messages.content4)}
              </p></div>
            </div>
          </div>
          <div className="col-4 ">
            <div className="row col-12   p-3">
              <i className="col-2 fa fa-check  fa-2x bg-beige d-flex align-items-center"></i>
              <div className="col-10"><label>
                {formatMessage(messages.content5_title)}
              </label>
              <p>
                {formatMessage(messages.content5)}
              </p></div>
            </div>
          </div>
        </div>
    </React.Fragment>

  }

}

export default injectIntl(Home);
