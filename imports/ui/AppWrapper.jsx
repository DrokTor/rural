import React, { Component } from 'react';
import { IntlProvider, addLocaleData, injectIntl } from "react-intl";
import deLocaleData from "react-intl/locale-data/de";
import enLocaleData from "react-intl/locale-data/en";
import translations from "../../src/i18n/locales";

import App from "./App";

addLocaleData(deLocaleData);
addLocaleData(enLocaleData);

class AppWrapper extends Component {


  render() {

    const messages = translations["en"];

    return (
      <IntlProvider locale="en"  messages={messages}>
        <App />
      </IntlProvider>
    );
  }
}

export default AppWrapper;
