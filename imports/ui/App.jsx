import React, {Component} from 'react';
import { BrowserRouter,Route,Redirect } from 'react-router-dom'
import { ApolloProvider } from "react-apollo";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import { Session } from 'meteor/session';
import Header from './Header.jsx';
import Footer from './Footer.jsx';
import Menu from './Menu.jsx';
import Products from './Products.jsx';
import PopProducts from './PopProducts.jsx';
import ShopList from './ShopList.jsx';
import ShowProduct from './ShowProduct.jsx';
import Login from './account/Login.jsx';
import NewUser from './account/NewUser.jsx';
import Profile from './account/Profile.jsx';
import PasswordForgotten from './account/PasswordForgotten.jsx';
import PasswordReset from './account/PasswordReset.jsx';
import EmailVerified from './account/EmailVerified.jsx';
import Store from './Store.jsx';
import Home from './Home.jsx';
import Purchases from './Purchases.jsx';
import TopReviews from './TopReviews.jsx';
import Client from '../api/apolloClient';
import { Accounts } from 'meteor/accounts-base';
import 'typeface-raleway';
import { injectIntl, defineMessages } from "react-intl";
import arLocaleData from "react-intl/locale-data/en";
import esLocaleData from "react-intl/locale-data/de";

const GET_WISH_SHOPLIST = gql`
query Item{
  wishList @client
  shopList @client{
    _id
    qty
  }
  cost @client
}
`;



const UPDATE_SHOPLIST = gql`
  mutation updateShopList($shopList: [Item_qty]){
    updateShopList(shopList:$shopList) @client
  }
`;

class App extends Component {

  constructor(props){
    super(props);

    this.state={
      wishList: null,
      shopList: null,
      cost: 0,
      imageState: 0 ,
      validated: false ,
    }

    this.initWishList = this.initWishList.bind(this);
    this.initShopList = this.initShopList.bind(this);
    this.handleWish = this.handleWish.bind(this);
    this.handleShop = this.handleShop.bind(this);
    this.handleImage = this.handleImage.bind(this);
    this.handleOrder = this.handleOrder.bind(this);
    this.handleCost = this.handleCost.bind(this);
  }


  initWishList(client,wishList){
    // console.log(wishList)
    // console.log(wishList.length>0 &&  this.state.wishList!=[])
    if(wishList && wishList.length>0 &&  !this.state.wishList){
      this.setState({wishList})
    }else if(wishList && wishList.length===0) {
      this.setState({wishList:[]})
    }else{
      //console.log(shopList)
      client.writeData({data: {wishList:[]}});
      //console.log('initShop');
    }
  }
  initShopList(client,shopList,cost){
    // console.log(wishList.length>0 &&  this.state.wishList!=[])
    if(shopList && shopList.length>0 &&  !this.state.shopList){
      this.setState({shopList,cost})
    }else if(shopList && shopList.length===0) {
      this.setState({shopList:[]})
    }else{
      console.log(shopList)
      client.writeData({data: {shopList:[],cost:0}});
      console.log('initShop');
    }
  }
  handleWish(client,data,elem){

    //wishList = [ ];

    let wishList = [...data.wishList];
    if(wishList.indexOf(elem) === -1){
      wishList.push(elem);
    }else {
      wishList.splice(wishList.indexOf(elem), 1);
    }
    this.setState({wishList});
    client.writeData({ data:  {wishList}  });

  }
  handleShop(client,data,_id,price,qty='1'){
    // console.log('shop');
    //console.log(data);
      let shopList =  [...data.shopList] ;
      let cost = 0;
       //let shopList = data.shopList;
      cost = data.cost;
      //  console.log(Client.readQuery({ query:GET_WISH_SHOPLIST }));
      //   console.log(shopList);
      //  console.log(data.cost);
      // console.log(shopList.findIndex(x=>x._id === _id));
      //if(shopList.indexOf(_id) === -1){
      if(shopList.findIndex(x=>x._id === _id)=== -1){
        shopList.push({__typename:"ShopList",_id,qty});
        cost += parseFloat(price)*qty; // qty not used ! always adding one product at a time
      }else {
        // console.log('dddddddddd');
        //shopList.splice(shopList.indexOf(_id), 1);
        shopList.splice(shopList.findIndex(x=>x._id === _id), 1);
        cost -= parseFloat(price)*qty;
      }
      this.setState({shopList,cost,validated:false});
     //console.log(shopList);
    // console.log(client.writeQuery);
    // console.log(client.mutate);
    // console.log(client.mutation);
    //client.writeData({ data:  {shopList:[  "68woyRTAQie3WBnMd"  ]}   });
      client.writeData({ data:   {shopList,cost}   });
      //client.writeData({ data:   {shopList}   });
      //client.writeData({ data:   {shopList: shopList}   });
    //client.writeQuery({UPDATE_SHOPLIST},{ data:  {shopList,cost}  });
    //client.mutate({mutation:UPDATE_SHOPLIST, variables:{shopList}})
  }
  // handleWish(elem){
  //     let wishList = this.state.wishList;
  //     //const elem = event.target.dataset.id;
  //     if(wishList.indexOf(elem) === -1){
  //       wishList.push(elem);
  //     }else {
  //       wishList.splice(wishList.indexOf(elem), 1);
  //     }
  //     this.setState({wishList});
  //     Session.set({wishList});
  //     console.log(Session.get('wishList'));
  // }

  // handleShop(elem,price){
  //   console.log((price));
  //   console.log(Number(price));
  //   console.log(parseInt(price));
  //     let shopList = this.state.shopList;
  //     let cost = this.state.cost;
  //     //const elem = event.target.dataset.id;
  //     if(shopList.indexOf(elem) === -1){
  //       shopList.push(elem);
  //       cost += parseInt(price);
  //     }else {
  //       shopList.splice(shopList.indexOf(elem), 1);
  //       cost -= parseInt(price);
  //     }
  //     this.setState({shopList,cost});
  // }

  handleImage(index){
    this.setState({imageState : index});
  }

  handleOrder(){
    Client.writeData({data: {shopList:[],cost:0}});
    this.setState({shopList:[],cost:0,validated:true});
  }
  handleCost(newCost){
    console.log(newCost);
    //Client.writeData({data: {cost:newCost}});
    this.setState({ cost:newCost });
  }

  render()
  {

     //console.log(this.state.wishList);
     //console.log(Session.get('wishList'));
  return  <ApolloProvider  client={Client}>
      <BrowserRouter>
        <div className="pt-no-menu  h-100 min-height-500 ">
          {/* <Header />*/}
          <Query query={GET_WISH_SHOPLIST}
            // onCompleted={(data,client)=> {
            //     //alert(!this.state.shopList)
            // }}
            // fetchPolicy={"cache-and-network"}
            >
            {({ data, client }) => {
              !this.state.wishList && this.initWishList( client,data.wishList)
              !this.state.shopList && this.initShopList(client,data.shopList,data.cost)
                  //client.writeData({ data:  {wishList:[],shopList:[],cost:0}  });
               //console.log(data);
              return true;
            }}
          </Query>
          <Route
            path="/"
            component={props => <Header {...props} initWishList={this.initWishList} wishList = {this.state.wishList} shopList = {this.state.shopList} />} //extra={someVariable}
          />
          <Route path="/(|products|product)" component={Menu}/>

          <Route exact path="/" component={Home}/>
          <Route
            exact path="/"
            component={props => <div><PopProducts {...props}  handleWish = {this.handleWish} handleShop = {this.handleShop} wishList={this.state.wishList} shopList={this.state.shopList} /></div>} //extra={someVariable}
          />
          <Route
            exact path="/"
            component={props => <TopReviews {...props}    />} //extra={someVariable}
          />
          <Route
            exact path="/wishlist"
            component={props => <Products {...props}  handleWish = {this.handleWish} handleShop = {this.handleShop} wishList={this.state.wishList} shopList={this.state.shopList || []} showWishList={this.state.wishList} />} //extra={someVariable}
          />
          <Route
            exact path="/products/:cat?/:fam?/:avail?/:price?"
            component={props => <Products {...props}  handleWish = {this.handleWish} handleShop = {this.handleShop} wishList={this.state.wishList} shopList={this.state.shopList || []}  />} //showWishList={this.state.wishList} extra={someVariable}
          />
          <Route
            exact path="/shoplist"
            component={props => <ShopList {...props}  handleOrder={this.handleOrder} validated={this.state.validated} handleWish = {this.handleWish} handleShop = {this.handleShop} wishList={this.state.wishList} shopList={this.state.shopList } showShopList={this.state.shopList} cost ={ this.state.cost } handleCost={this.handleCost} />} //extra={someVariable}
          />
          <Route
            exact path="/product/:id"
            component={props => <ShowProduct {...props}  handleWish = {this.handleWish} handleShop = {this.handleShop} wishList={this.state.wishList || [] } shopList={this.state.shopList || [] } showShopList={this.state.shopList}  imageState={this.state.imageState} handleImage={this.handleImage}/>} //extra={someVariable}
          />
          <Route path="/login" component={Login}/>
          <Route render={({location}) => {
            // console.log(location.pathname);
            const loginCondition = !Meteor.userId() &&
            !!location.pathname.match(/^(\/user(\/profile))(.*)$/);

            //console.log(loginCondition);
            return loginCondition ? <Redirect to={{pathname: '/login'}} /> : ''
          }} />
          <Route path="/user/new" component={NewUser}/>
          <Route path="/user/profile" component={Profile}/>
          <Route path="/user/forgot" component={PasswordForgotten}/>
          <Route path="/user/reset" component={PasswordReset}/>
          <Route path="/user/verifyEmail/:t?" component={EmailVerified}/>
          <Route
            path="/store"
            component={props =>{
              if(Meteor.userId()){
                return <Store {...props} />  //extra={someVariable}
              }else{
                return <Login {...props}/>
              }
            }}
          />
          <Route
            path="/purchases"
            component={props =>{
              if(Meteor.userId()){
                return <Purchases {...props} />  //extra={someVariable}
              }else{
                return <Login {...props}/>
              }
            }}
          />
          {/* <Route
            path="/"
            component={props => <Footer {...props} initWishList={this.initWishList}  />} //extra={someVariable}
          /> */}
          <Route render={({location}) => {
            //console.log(location.pathname);
            //console.log(!location.pathname.match(/^(\/store)(.*)$/));
            return !location.pathname.match(/^(\/store)(.*)$/) ? <Footer      /> : ''
          }} />
          {/* <Route path="/store" component={Store}/> */}

       </div>
      </BrowserRouter>
    </ApolloProvider>

  }

}
export default injectIntl(App);
