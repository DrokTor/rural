import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import gql from "graphql-tag";
import { Query,Mutation } from "react-apollo";
import { Link,BrowserRouter,Route } from 'react-router-dom'
import { Button, Col,MDBDatePicker, Container } from 'mdbreact';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import {Doughnut, Bar, Line,defaults} from 'react-chartjs-2';
import { injectIntl, defineMessages } from "react-intl";
import { _ } from 'lodash';



const chartOptions =  {
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            }
        }]
    }
}
const chartOptions2 =  {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }],
            xAxes: [{
                ticks: {
                    display: false //this will remove only the label
                }
            }]
        }
    }
const GET_ITEMS = gql`
  query getItems($id: ID, $state: String  ){
    getItems(order_id: $id, state: $state  )  {
      _id,
      # seller_id,
      # order_id,
      product_id,
      product_name,
      # name,
      # img,
      # buyer_id,
      date,
      qty,
      # state,
      # error,
    }
  }
`;





class Dashboard extends Component {


constructor(props){
  super(props);

  this.state = {

  };

  this.handleChange = this.handleChange.bind(this);

}


  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,

    });
  }




 render() {





   return <div className="row mt-1 col-12  ">
            <div className="col-6">
            <Query
              query={GET_ITEMS}
              fetchPolicy={"cache-and-network"}
              >
              {({data,loading,error})=>{

                let groupedData = _
                  .chain(data.getItems)
                  .groupBy(datum => (moment(datum.date).format('MMM')) )
                  .map((item, itemId)=> {
                    var obj = {label : itemId};
                    //console.log(item);
                    obj['value'] = _.sumBy(item, (it)=>{
                      //console.log(item.qty);

                      return parseInt(it.qty)})
                    return obj
                  })
                  .value()


                let orderMonth = [];
                moment.monthsShort().map((date,i)=>{
                   const el = groupedData.find(obj=>obj.label===date);
                  //console.log(el);
                   orderMonth[i] = el? el.value: 0;
                });
                let data2= {
                     // labels: _.map(groupedData,'labels'),
                     labels: moment.monthsShort(),
                     datasets: [{
                         label: 'Number of orders per month',
                         fill: false,
                         data: orderMonth,
                         borderColor: 'rgb(3, 192, 233)',
                         borderWidth: 1
                     }],
                     //options : chartOptions,
                 };
                return <Line data={data2}  options={chartOptions} />;
              }}
            </Query>
            </div>
            <div className="col-6">
            <Query
              query={GET_ITEMS}
              //fetchPolicy={"cache-and-network"}
              >
              {({data,loading,error})=>{

                let groupedData = _
                  .chain(data.getItems)
                  .groupBy(datum => (moment(datum.date).format('MMM')) )
                  .map((item, itemId)=> {
                    var obj = {label : itemId};
                    //console.log(item);
                    obj['value'] = _.sumBy(item, (it)=>{
                      //console.log(item.qty);

                      return parseInt(it.qty)})
                    return obj
                  })
                  .value()
                let soldProducts = _
                  .chain(data.getItems)
                  .countBy((item)=>{
                    //console.log(item);
                    return item.product_name;
                  })
                  .value();

                  //console.log(soldProducts);

                // let orderMonth = [];
                // moment.monthsShort().map((date,i)=>{
                //    const el = soldProducts.find(obj=>obj.label===date);
                //   //console.log(el);
                //    orderMonth[i] = el? el.value: 0;
                // });
                let data2= {
                     labels: Object.keys(soldProducts),
                     datasets: [{
                         label: 'Total of products ordered',
                         fill: false,
                         data: Object.values(soldProducts),
                         borderColor: 'rgb(171, 146, 191)',
                         backgroundColor: 'rgb(208, 229, 232)',
                         borderWidth: 1
                     }],
                     //options : chartOptions,
                 };
                return <Bar data={data2}  options={chartOptions2} />

              }}
            </Query>
            </div>
          </div>;

 }

}


export default injectIntl(Dashboard);
