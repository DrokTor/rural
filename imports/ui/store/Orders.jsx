import React, { Component } from 'react';
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
import { _ } from 'lodash';
//import Icon from '@material-ui/core/Icon';
import {Link} from "react-router-dom";
//import {DeleteIcon} from '@material-ui/icons/DeleteIcon';
import {Delete , ShoppingCart, AddShoppingCart, RemoveShoppingCart} from '@material-ui/icons';
import Modal from '@material-ui/core/Modal';
import { injectIntl, defineMessages } from "react-intl";

const messages = defineMessages({
  content1: {
    id: 'orders.content1',
    defaultMessage: 'Commandes'
  },
  content2: {
    id: 'orders.content2',
    defaultMessage: 'Quantité:'
  },
  content3: {
    id: 'orders.content3',
    defaultMessage: 'Detail'
  },
  content4: {
    id: 'orders.content4',
    defaultMessage: 'Produit:'
  },
  content6: {
    id: 'orders.content6',
    defaultMessage: 'Coût total:'
  },
  lastName: {
    id: 'profile.lastName',
    defaultMessage: 'Nom:'
  },
  firstName: {
    id: 'profile.firstName',
    defaultMessage: 'Prénom:'
  },
  phone: {
    id: 'profile.phone',
    defaultMessage: 'Téléphone:'
  },
  country: {
    id: 'profile.country',
    defaultMessage: 'Pays:'
  },
  city: {
    id: 'profile.city',
    defaultMessage: 'Ville:'
  },
  address: {
    id: 'profile.address',
    defaultMessage: 'Adresse:'
  },
  closedOrder: {
    id: 'orders.closedOrder',
    defaultMessage: 'Commande clôturée'
  },
  state: {
    id: 'orders.state',
    defaultMessage: 'État:'
  },
  waiting: {
    id: 'orders.waiting',
    defaultMessage: 'En attente de récéption ...'
  },
  accept: {
    id: 'orders.accept',
    defaultMessage: 'Accepter la commande'
  },
  send: {
    id: 'orders.send',
    defaultMessage: 'Envoyer la commande'
  },
  close: {
    id: 'orders.close',
    defaultMessage: 'Clôturer la commande'
  },
  envoyée: {
    id: 'state.sent',
    defaultMessage: 'envoyée'
  },
  commande: {
    id: 'state.order',
    defaultMessage: 'commande'
  },
  validée: {
    id: 'state.accepted',
    defaultMessage: 'validée'
  },
  reçue: {
    id: 'state.received',
    defaultMessage: 'reçue'
  },
  clôturée: {
    id: 'state.closed',
    defaultMessage: 'clôturée'
  },



})


const GET_PROFILE = gql`
  query GetProfile($id: ID) {
    getProfile(_id:$id  ){
      firstName
      lastName
      country
      city
      address
      phone
    }
  }
`;

const SET_ORDER = gql`
  mutation SetOrderItem($id: ID, $state: String! ) {
    setOrderItem(_id:$id, state:$state ){
      _id
      buyer_id
      date
      product_id
      state
    }
  }
`;

const GET_ORDERS = gql`
  query getMyOrders($id: ID, $buyer: Boolean = false){
    getMyOrders(_id: $id, buyer: $buyer )  {
      _id
      buyer_id
      date
      items
        {
          product_id
          name
          img
          seller_id
          qty
          cost
          state
          error
        }
    }
  }
`;
const GET_ITEMS = gql`
  query getItems($id: ID, $state: String  ){
    getItems(order_id: $id, state: $state  )  {
      _id,
      seller_id,
      order_id,
      product_id,
      name,
      img,
      buyer_id,
      date,
      qty,
      cost,
      state,
      error,
    }
  }
`;

class Orders extends Component {

  constructor(props){
    super(props);

    this.state={
      order: false,
      state: '',
    }

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleOpen(src,item){
    //console.log(item._id);
    this.setState({
      src,
      _id: item._id,
      product_id: item.product_id,
      name: item.name,
      qty: item.qty,
      cost: item.cost,
      state: item.state,
      open: true,
      });
  };

  handleClose(){
    this.setState({ open: false });
  };


  render(){

    const {intl:{formatMessage}} = this.props;


    let variables = {};
    if( this.state.state ){
      variables.state  = this.state.state ;
    }


    return <div className="row col-11 mx-auto ">


     <Query
      query={GET_ITEMS}
      fetchPolicy={"cache-and-network"}
      //fetchPolicy={"network-only"}
      variables={variables}
      //variables={{id : ["CJ2gth3WxgyQx2DCv"]  }}
      //pollInterval={2000}
      >
      {({ loading, error, data, variables }) => {
        // console.log(variables);
        if (loading) return <p>Loading...</p>;
        if (error) return <p>Error :(</p>;
        //random image remove for product
        let url = Meteor.absoluteUrl();
        url = url.split('://');
        url[1] = url[1].split(':');
        const baseurl = url[1][0].replace(/\/$/, ""); // remove trailing slash;

        url  = url[0]+"://"+baseurl+':3080/';

        //console.log(data.getMyOrders);
        // const orders =  _.groupBy(data.getMyOrders,"order_id");
        // console.log(orders);
        const orders= data.getItems;
        //console.log(orders);

        return <React.Fragment>
          <div className="row col-12 mb-5 ">
            <h3 className="col-3">{formatMessage(messages.content1)}</h3>
            <div className="row col-9">
              <div className="col-2  ">
                <button className="btn border col-12  bg-white text-secondary"
                        onClick={()=>this.setState({state:"commande"})}>
                  <i className="fa    fa-clock-o" ></i>
                </button>
              </div>
              <div className="col-2">
                <button className="btn border col-12  bg-white text-secondary"
                        onClick={()=>this.setState({state:"validée"})}>
                  <i className="fa    fa-arrow-down" ></i>
                </button>
              </div>
              <div className="col-2">
                <button className="btn border col-12  bg-white text-secondary"
                        onClick={()=>this.setState({state:"envoyée"})}>
                  <i className="fa    fa-arrow-up" ></i>
                </button>
              </div>
              <div className="col-2">
                <button className="btn border col-12  bg-white text-secondary"
                        onClick={()=>this.setState({state:"reçue"})}>
                  <i className="fa    fa-check" ></i>
                </button>
              </div>
              <div className="col-2">
                <button className="btn border col-12  bg-white text-secondary"
                        onClick={()=>this.setState({state:"clôturée"})}>
                  <i className="fa  fa-stop  " ></i>
                </button>
              </div>
              <div className="col-2">
                <button className="btn border col-12  bg-white text-secondary"
                        onClick={()=>this.setState({state:""})}>
                  <i className="fa  fa-times  " ></i>
                </button>
              </div>
            </div>
          </div>
         { orders.map((item,index)=>(
          <div key= {index} className="row col-12 mb-1 border-bottom p-1 d-flex align-items-center text-center">
            <div className="col-1 p-0">
              <img className="col-12 image-fluid p-0" src={url+item.product_id+"/"+item.img}/>
            </div>
            <div  className=" col-4 pl-3">{item.name}</div>
            <div  className="col-3 p-0"><label>{formatMessage(messages.content2)}</label> {item.qty}</div>
            {item.state== "reçue" && <div  className="col-3 p-0">  {formatMessage(messages[item.state])} <i className="fa  fa-check text-warning" ></i> </div>}
            {item.state== "clôturée" && <div  className="col-3 p-0">  {formatMessage(messages[item.state])} <i className="fa  fa-stop text-info" ></i> </div>}
            {item.state!= "clôturée" && item.state!= "reçue" && <div  className="col-3 p-0">  {formatMessage(messages[item.state])} <i className={"fa  "+(item.state == "commande" ? "text-danger fa-clock-o": (item.state == "validée" ? "text-success fa-arrow-down":"text-primary fa-arrow-up")) } ></i></div>}
            <div  className="col-1 p-0">
              <button className="btn btn-outline-success border float-right"
                 onClick={()=>{this.handleOpen(url+item.product_id+"/"+item.img,item)}}>{formatMessage(messages.content3)} <i className="fa fa-info-circle"></i></button>
            </div>
          </div>
        ))
       }
        </React.Fragment>
      }}
    </Query>
    <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
      <div   className="row col-8 mx-auto bg-light mt-5 border rounded p-5">
        <div className="row col-12 mb-4">
          <div className="col-4">
            <div className="col-12  "><img className="col-12 p-0" src={this.state.src} /></div>
          </div>
          <div className="col-8">
            <div className="col-12  "><label className="col-3 pl-0">{formatMessage(messages.content4)} </label>{this.state.name}</div>
            <div className="col-12  "><label className="col-3 pl-0">{formatMessage(messages.content2)} </label>{this.state.qty}</div>
            <div className="col-12  "><label className="col-3 pl-0">{formatMessage(messages.content6)} </label>{this.state.cost}</div>
            <Query
              query={GET_PROFILE}
              variables={{id: this.state.buyer_id}}
              fetchPolicy={'cache-and-network'}
              >
              {({data,error,loading})=>{
                if(loading){
                  return "loading";
                }
                if(error){
                  console.log(error);
                  return "error";
                }
                const profile = data.getProfile;
                //console.log(profile);
                return <React.Fragment>

                  <div className="col-12  mt-3"><label className="col-3 pl-0" >{formatMessage(messages.lastName)} </label>{profile.lastName}</div>
                  <div className="col-12  "><label className="col-3 pl-0" >{formatMessage(messages.firstName)} </label>{profile.firstName}</div>
                  <div className="col-12  "><label className="col-3 pl-0" >{formatMessage(messages.phone)} </label>{profile.phone}</div>
                  <div className="col-12  "><label className="col-3 pl-0" >{formatMessage(messages.country)} </label>{profile.country}</div>
                  <div className="col-12  "><label className="col-3 pl-0" >{formatMessage(messages.city)} </label>{profile.city}</div>
                  <div className="col-12  "><label className="col-3 pl-0" >{formatMessage(messages.address)} </label>{profile.address}</div>
                </React.Fragment>
              }}
            </Query>

          </div>
        </div>
        <hr className="col-12"/>
        <div className="col-12 p-0 mt-4">
          <Mutation
            mutation={SET_ORDER}
            // variables={variables}
            onCompleted={ data => {
              console.log(data);
              this.setState({state: data.setOrderItem.state});
              }
            }
            // update={(cache, { data: { addProduct } }) => {
            //    //console.log(addProduct);
            //    if(cache.data.data.ROOT_QUERY.getProducts){
            //         //console.log(cache.data.data.ROOT_QUERY);
            //         const products  = cache.readQuery({ query: GET_PRODUCTS });
            //         //console.log(products);
            //         cache.writeQuery({
            //           query: GET_PRODUCTS,
            //           data: { getProducts: products.getProducts.concat([addProduct]) }
            //         });
            //    }
            //  }}
             >
            {(setOrder, { loading, error,data }) => {
              //if(data) this.setState({state: data.setOrderItem.state});

            if(this.state.state == "clôturée")
            return  <div className="row col-12 p-0 m-0">
                      <div className=" p-4 bg-warning col-6 mx-auto text-center">
                        {formatMessage(messages.closedOrder)} <i className="fa fa-check"></i><i className="fa fa-check"></i>
                      </div>
                    </div>
            return <div className="row col-12 p-0 m-0">
              <div className="row col-4 m-0  d-flex align-items-center">
                <div className="font-weight-bold col-4">{formatMessage(messages.state)}</div>
                <div className={"font-weight-bold col-8 border rounded p-3 text-center "+(this.state.state == "commande" ? "border-danger text-danger": (this.state.state == "validée" ? " border-success text-success":" border-primary text-primary") )}
                  >{formatMessage(messages[this.state.state])}</div>
              </div>
              { this.state.state !="envoyée" &&  <button className={"col-lg-4 offset-lg-4 col-sm-5 offset-sm-3 btn  d-flex align-items-center "+(this.state.state == "commande" ? "btn-success": (this.state.state == "validée" ? " btn-danger":" btn-warning ") )}
                onClick={()=>{setOrder({variables:{id:this.state._id,product_id:this.state.product_id,state:this.state.state}})}}
                ><div className="col-10">{(this.state.state == "commande" ? formatMessage(messages.accept): (this.state.state == "validée" ? formatMessage(messages.send):formatMessage(messages.close)))}</div>
                 <i className={"fa "+(this.state.state == "commande" ? "fa-arrow-down fa-2x ": (this.state.state == "validée" ? "fa-arrow-up fa-2x ":"fa-window-close fa-2x"))}></i></button>}
              { this.state.state =="envoyée" && <div className= "col-lg-4 offset-lg-4 col-sm-5 offset-sm-3    d-flex align-items-center text-danger" >
                        {formatMessage(messages.waiting)}
              </div>}
            </div>
            }}
          </Mutation>
        </div>
      </div>
    </Modal>
    </div>

  }

}

export default injectIntl(Orders);
