import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Query,Mutation } from 'react-apollo';
import gql from "graphql-tag";
import Client from '../../api/apolloClient';
import { Link,BrowserRouter,Route } from 'react-router-dom'
import { Button, Col,MDBDatePicker, Container } from 'mdbreact';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import Upload from './Upload';
import Typography from '@material-ui/core/Typography';
import RichTextEditor from 'react-rte';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import { injectIntl, defineMessages } from "react-intl";

const messages = defineMessages({
  photo: {
    id: 'product.photo',
    defaultMessage: 'Photo'
  },
  name: {
    id: 'product.name',
    defaultMessage: 'Nom du produit'
  },
  city: {
    id: 'product.city',
    defaultMessage: 'Ville'
  },
  category: {
    id: 'product.category',
    defaultMessage: 'Catégorie'
  },
  family: {
    id: 'product.family',
    defaultMessage: 'Famille'
  },
  weight: {
    id: 'product.weight',
    defaultMessage: 'Poids (kg)'
  },
  price: {
    id: 'product.price',
    defaultMessage: 'Prix (DZ)'
  },
  edit: {
    id: 'product.edit',
    defaultMessage: 'Modifier'
  },
  availability: {
    id: 'product.availability',
    defaultMessage: 'Disponibilité'
  },
  qtyAvailable: {
    id: 'product.qtyAvailable',
    defaultMessage: 'Quantité disponible'
  },
  prepTime: {
    id: 'product.prepTime',
    defaultMessage: 'Jours de préparation'
  },
  available: {
    id: 'product.available',
    defaultMessage: 'Disponible'
  },
  byOrder: {
    id: 'product.byOrder',
    defaultMessage: 'Sur commande'
  },
  description: {
    id: 'product.description',
    defaultMessage: 'Description'
  },
  add: {
    id: 'product.add',
    defaultMessage: 'Ajouter un nouveau produit'
  },
  addBtn: {
    id: 'product.addBtn',
    defaultMessage: 'Ajouter'
  },
  updateBtn: {
    id: 'product.updateBtn',
    defaultMessage: 'Modifier'
  },
  addAnother: {
    id: 'product.addAnother',
    defaultMessage: 'Ajouter un autre produit'
  },
  editProduct: {
    id: 'product.editProduct',
    defaultMessage: 'Modifier un produit'
  },
  added: {
    id: 'product.added',
    defaultMessage: 'Produit ajouté avec succès !'
  },
  updated: {
    id: 'product.updated',
    defaultMessage: 'Produit modifié avec succès !'
  },
  error: {
    id: 'product.error',
    defaultMessage: 'Une erreur s\'est produite lors de l\'ajout du produit !'
  },
  confirm: {
    id: 'product.confirm',
    defaultMessage: 'Confirmer'
  },
  deleteMsg: {
    id: 'product.deleteMsg',
    defaultMessage: 'Êtes-vous sûr de vouloir supprimer la photo ?'
  },
  deleteOui: {
    id: 'product.deleteOui',
    defaultMessage: 'Oui'
  },
  deleteNon: {
    id: 'product.deleteNon',
    defaultMessage: 'Non'
  },
})

const wilayas = [{"code":"1","nom":"Adrar"}, {"code":"2","nom":"Chlef"}, {"code":"3","nom":"Laghouat"}, {"code":"4","nom":"Oum El Bouaghi"}, {"code":"5","nom":"Batna"}, {"code":"6","nom":"B\u00e9ja\u00efa"}, {"code":"7","nom":"Biskra"}, {"code":"8","nom":"B\u00e9char"}, {"code":"9","nom":"Blida"}, {"code":"10","nom":"Bouira"}, {"code":"11","nom":"Tamanrasset"}, {"code":"12","nom":"T\u00e9bessa"}, {"code":"13","nom":"Tlemcen"}, {"code":"14","nom":"Tiaret"}, {"code":"15","nom":"Tizi Ouzou"}, {"code":"16","nom":"Alger"}, {"code":"17","nom":"Djelfa"}, {"code":"18","nom":"Jijel"}, {"code":"19","nom":"S\u00e9tif"}, {"code":"20","nom":"Sa\u00efda"}, {"code":"21","nom":"Skikda"}, {"code":"22","nom":"Sidi Bel Abb\u00e8s"}, {"code":"23","nom":"Annaba"}, {"code":"24","nom":"Guelma"}, {"code":"25","nom":"Constantine"}, {"code":"26","nom":"M\u00e9d\u00e9a"}, {"code":"27","nom":"Mostaganem"}, {"code":"28","nom":"M'Sila"}, {"code":"29","nom":"Mascara"}, {"code":"30","nom":"Ouargla"}, {"code":"31","nom":"Oran"}, {"code":"32","nom":"El Bayadh"}, {"code":"33","nom":"Illizi"}, {"code":"34","nom":"Bordj Bou Arreridj"}, {"code":"35","nom":"Boumerd\u00e8s"}, {"code":"36","nom":"El Tarf"}, {"code":"37","nom":"Tindouf"}, {"code":"38","nom":"Tissemsilt"}, {"code":"39","nom":"El Oued"}, {"code":"40","nom":"Khenchela"}, {"code":"41","nom":"Souk Ahras"}, {"code":"42","nom":"Tipaza"}, {"code":"43","nom":"Mila"}, {"code":"44","nom":"A\u00efn Defla"}, {"code":"45","nom":"Na\u00e2ma"}, {"code":"46","nom":"A\u00efn T\u00e9mouchent"}, {"code":"47","nom":"Gharda\u00efa"}, {"code":"48","nom":"Relizane"}];



const cat_list=[
  {
    name: "Bijoux",
    name_tr: "Jewelry",
    id_tr:"menu.jewelry",
    family: [{name:"Coliers",name_tr:"necklaces",id_tr: 'menu.content1_1'},{name:"Bracelets",name_tr:"braclets",id_tr: 'menu.content1_2'},{name:"Anneaux",name_tr:"rings",id_tr: 'menu.content1_3'},{name:"Boucles d'oreilles",name_tr:"earrings",id_tr: 'menu.content1_4'}]
  },
  {
    name: "Vêtements",
    name_tr: "Clothings",
    id_tr:"menu.clothings",
    family: [{name:"Écharpes",name_tr:"scarves",id_tr: 'menu.content2_1'},{name:"chemises",name_tr:"shirts",id_tr: 'menu.content2_2'},{name:"Gants",name_tr:"gloves",id_tr: 'menu.content2_3'}]
  },
  {
    name: "Maison",
    name_tr: "Home",
    id_tr:"menu.home",
    family: [{name:"Tapis",name_tr:"rugs",id_tr: 'menu.content3_1'},{name:"Cadres",name_tr:"paintings",id_tr: 'menu.content3_2'},{name:"Vases",name_tr:"vases",id_tr: 'menu.content3_3'}]
  },
  {
    name: "Ustensiles",
    name_tr: "Utensils",
    id_tr:"menu.utensils",
    family: [{name:"Tasses",name_tr:"mugs",id_tr:"menu.content4_1"},{name:"Plats",name_tr:"dishes",id_tr:"menu.content4_2"},{name:"Cuillères",name_tr:"spoons",id_tr:"menu.content4_3"}]
  },

];

cat_list.map((cat,index)=>{

  messages['cat_'+cat.name_tr]={
    id: cat.id_tr,
    defaultMessage: cat.name
  };

  cat.family.map((fam,ind)=>{
    messages[cat.name+'_fam_'+ind]={
      id: fam.id_tr,
      defaultMessage: fam.name
      }
  })
})


//console.log(messages);
const DELETE_FILE = gql`
  mutation DeleteFile($id:ID!, $name:String!){
    deleteFile(_id:$id,name:$name)
  }
`
const ADD_PRODUCT = gql`
  mutation AddProduct($_id: ID ,$name: String!, $city: String!, $category: String!, $family: String! , $price: Int!, $weight: String!,$availability: String!, $qtyAvailable: Int,$prepTime: Int, $description: String!, $files:[Upload!]!) {
    addProduct(_id:$_id,name:$name, city:$city,category:$category,family:$family,availability: $availability, qtyAvailable: $qtyAvailable ,prepTime: $prepTime , price:$price,weight:$weight, description: $description, files: $files){
      _id
      name
      city
      category
      family
      weight
      price
      description
      availability
      qtyAvailable
      prepTime
      filenames
    }
  }
`;

const GET_PRODUCTS = gql`
  query getProducts($id: [ID], $cat: String, $fam: String, $price: String){
    getProducts(_id: $id,category: $cat,family: $fam,price: $price)  {
      _id
      name
      category
      family
      city
      weight
      price
      availability
      qtyAvailable
      prepTime
      description
      filenames
    }
  }
`;
// const GET_PRODUCTS=gql`
//   {
//     getProducts  {
//       _id
//       name
//       city
//       category
//       family
//       weight
//       price
//     }
//   }
// `;

class NewProduct extends Component {


constructor(props){
  super(props);

  this.state = {
    _id: this.props.match.params.id || '',
    name: '',
    category: '',
    family: '',
    type: '',
    city: '',
    price: 0,
    weight: '',
    ingredients: '',
    files: '',
    filenames: '',
    availability: '',
    qtyAvailable: 0,
    prepTime: 0,
    description: '',//RichTextEditor.createEmptyValue(),
    description: '',//RichTextEditor.createEmptyValue(),
    desc_value: RichTextEditor.createEmptyValue(),
    added: false,
    open: false,
    errorShowed: false,
    vertical: 'bottom',
    horizontal: 'right',
    user_id : Meteor.userId(),
  };

  this.handleChange = this.handleChange.bind(this);
  this.handleClick = this.handleClick.bind(this);
  this.handleClose = this.handleClose.bind(this);
  this.handleAdd = this.handleAdd.bind(this);
  this.handleAnother = this.handleAnother.bind(this);
  this.handleEditor = this.handleEditor.bind(this);
  this.htmlEscape = this.htmlEscape.bind(this);
  this.htmlUnescape = this.htmlUnescape.bind(this);
  //this.handleUploadDone = this.handleUploadDone.bind(this);
}


  async componentDidMount() {
    const {data} = await Client.query({query:GET_PRODUCTS,fetchPolicy:'network-only',variables:{id:this.props.match.params.id || 0}});
    const product = await data.getProducts[0];
    // console.log(data.getProducts[0]);
     //console.log(product);
    if(product)
    {
      this.setState({
      name : product.name,
      category : product.category,
      family : product.family,
      city : product.city,
      weight : product.weight,
      availability : product.availability,
      qtyAvailable : product.qtyAvailable,
      prepTime : product.prepTime,
      description : product.description,
      desc_value : RichTextEditor.createValueFromString(this.htmlUnescape(product.description), 'html'),
      price : product.price,
      filenames : product.filenames,
      })
    }
  }

  htmlUnescape(str){

      return !str? '': str
          .replace(/&quot;/g, '"')
          .replace(/&#39;/g, "'")
          .replace(/&lt;/g, '<')
          .replace(/&gt;/g, '>')
          .replace(/&amp;/g, '&') ;
  }

  handleEditor(value){
    this.setState({desc_value : value});
    this.setState({description : this.htmlEscape(value.toString('html'))});

  };


  handleChange(event,name){
    this.setState({
      [name]: (name==="price" || name==="qtyAvailable" || name==="prepTime")?parseFloat(event.target.value):event.target.value,

    });
  }

  handleClick(state) {
    //console.log("state");
    this.setState({ open: true, ...state });
    //this.setState({ open: true, ...state });
  }
  handleAdd() {
    this.setState({ added: true });
  }
  // handleUploadDone() {
  //   console.log('dd');
  //   this.setState({ uploadDone: true, productId: '' });
  // }
  handleAnother() {

    this.setState({ name:'',price: 0, weight:'',added: false,errorShowed: true });
  }

  handleClose = () => {
    this.setState({ open: false });
  }

  htmlEscape(str) {
    return str
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
  }

 render() {

   const {intl:{formatMessage}} = this.props;


   let url = Meteor.absoluteUrl();
   url = url.split('://');
   url[1] = url[1].split(':');
   const baseurl = url[1][0].replace(/\/$/, ""); // remove trailing slash;

   url  = url[0]+"://"+baseurl+':3080/';

   const toolbarConfig = {
     // Optionally specify the groups to display (displayed in the order listed).
     display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'LINK_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'HISTORY_BUTTONS'],
     INLINE_STYLE_BUTTONS: [
       {label: 'Bold', style: 'BOLD', className: 'custom-css-class'},
       {label: 'Italic', style: 'ITALIC'},
       {label: 'Underline', style: 'UNDERLINE'}
     ],
     BLOCK_TYPE_DROPDOWN: [
       {label: 'Normal', style: 'unstyled'},
       //{label: 'Heading Large', style: 'header-one'},
       //{label: 'Heading Medium', style: 'header-two'},
       {label: 'Heading Small', style: 'header-five'}
     ],
     BLOCK_TYPE_BUTTONS: [
       {label: 'UL', style: 'unordered-list-item'},
       {label: 'OL', style: 'ordered-list-item'}
     ]
   };
   //console.log('Render New product');
   const { vertical, horizontal, open } = this.state;

   if(this.state.added ) //&& this.state.uploadDone
   return <div className="mx-auto mt-5 col-5 text-center">
            <div className=" alert alert-info">
              {!this.props.edit? formatMessage(messages.added):formatMessage(messages.updated)}
            </div>
            {!this.props.edit && <button className="btn btn-primary p-3" onClick={this.handleAnother}>{formatMessage(messages.addAnother)}</button>}
          </div>;

   return (
     <React.Fragment>
     <Mutation
     mutation={ADD_PRODUCT}
     onCompleted={ data => this.handleAdd() }
     updateBtn={(cache, { data: { addProduct } }) => {
        // console.log(addProduct);
        if(cache.data.data.ROOT_QUERY.getProducts){
             //console.log(cache.data.data.ROOT_QUERY);
             const products  = cache.readQuery({ query: GET_PRODUCTS });
             //console.log(products);
             cache.writeQuery({
               query: GET_PRODUCTS,
               data: { getProducts: products.getProducts.concat([addProduct]) }
             });
             // const productss  = cache.readQuery({ query: GET_PRODUCTS });
             // console.log(productss);
        }
      }}
     >

   {(addProduct, { loading, error,data }) => {
     if (loading){
       //console.log(data);
       return <div className="mx-auto mt-5 col-3"><CircularProgress   /></div>;
     }
     // if (data && !this.state.added && !this.state.productId){
     //
     //   this.setState({productId : data.addProduct._id});
     // }
     // if (data && !this.state.open){
     //   this.handleClick()
     //   console.log(data);
     // }
      if (error && !this.state.errorShowed ) {
        console.log(error);
        // this.setState({errorShowed : true});
        return <div className="mx-auto mt-5 col-6 text-center">
               <div className=" alert alert-danger">
                 {formatMessage(messages.error)}
               </div>
               <button className="btn btn-primary p-3" onClick={this.handleAnother}>{formatMessage(messages.addAnother)}</button>
             </div>;
           }

     //  if(data) {
     //
     //    return <div className="mx-auto mt-5 col-3 alert alert-info">Produit ajouté avec succès !</div>;
     //  }
     // if (!data)
     return <React.Fragment>
       <form className="row"
       onSubmit={e => {
              e.preventDefault();
              addProduct({ variables: this.state });
            }}
            >

      <h3 className="col-12 mb-5" >{!this.props.edit? formatMessage(messages.add):formatMessage(messages.editProduct)}</h3>
      <div className="col-4 mb-5">
       <TextField
          type="text"
          name=""
          autoFocus
          label={formatMessage(messages.name)}
          fullWidth
          value={this.state.name}
          onChange={event => this.handleChange(event,'name')}
          margin="none"
         />
       </div>
       <div className="col-4 mb-5">
        <TextField
          type="text"
          select
          name=""
          label={formatMessage(messages.category)}
          fullWidth
          value={this.state.category}
          onChange={event => this.handleChange(event,'category')}
          margin="none"
        >
          <MenuItem  value={""}>

          </MenuItem>
          {
            cat_list.map((elem,index)=>{
               //console.log(formatMessage(messages["cat_"+elem.name_tr]));
              return <MenuItem key={index} value={elem.name_tr}>
                {/* {elem.name} */}
                {formatMessage(messages["cat_"+elem.name_tr])}
              </MenuItem>
            })
          }


        </TextField>
      </div>
      <div className="col-4">
       <TextField
         type="text"
         select
         name=""
         label={formatMessage(messages.family)}
         fullWidth
         value={this.state.family }
         onChange={event => this.handleChange(event,'family')}
         margin="none"
         className="mb-5"
       >

         <MenuItem  value={""}>

         </MenuItem>
         { (()=>{
             let cat = cat_list.find(x=>x.name_tr === this.state.category);
             // console.log(cat);
             // console.log(this.state.category);
             if(cat){
               return cat.family.map((fam,index)=>{
                 return <MenuItem key={index}  value={fam.name_tr}>
                   {/* {fam.name} */}
                   {formatMessage(messages[cat.name+'_fam_'+index])}
                 </MenuItem>

               })
             }
           })()
         }
         </TextField>
     </div>
       <div className="col-4 mb-5">
        <TextField
          type="text"
          select
          name=""
          label={formatMessage(messages.city)}
          fullWidth
          value={this.state.city }
          onChange={event => this.handleChange(event,'city')}
          margin="none"
        >

        { wilayas.map( (val, index) =>{
          return  <MenuItem key={index}  value={val.nom}>
              {val.nom}
            </MenuItem>
          })
          }


        </TextField>
      </div>
       <div className="col-4">
        <TextField
          type="text"
          select
          name=""
          label={formatMessage(messages.availability)}
          fullWidth
          value={this.state.availability }
          onChange={event => this.handleChange(event,'availability')}
          margin="none"
        >
         <MenuItem  value={"available"}>
              {formatMessage(messages.available)}
         </MenuItem>
         <MenuItem  value={"by order"}>
              {formatMessage(messages.byOrder)}
         </MenuItem>
        </TextField>
      </div>
      <div className="col-4">
       { this.state.availability == 'available' ?
         <TextField
          type="number"
          name=""
          label={formatMessage(messages.qtyAvailable)}
          fullWidth
          value={this.state.qtyAvailable}
          onChange={event => this.handleChange(event,'qtyAvailable')}
          margin="none"
         />
         :
         <TextField
          type="number"
          name=""
          label={formatMessage(messages.prepTime)}
          fullWidth
          value={this.state.prepTime}
          onChange={event => this.handleChange(event,'prepTime')}
          margin="none"
         />
        }
       </div>

      <div className="col-4">
        <TextField
          type="text"
          name=""
          label={formatMessage(messages.weight)}
          fullWidth
          value={this.state.weight}
          onChange={event => this.handleChange(event,'weight')}
          margin="none"

          />
      </div>
      <div className="col-4">
       <TextField
          type="number"
          name=""
          label={formatMessage(messages.price)}
          fullWidth
          value={this.state.price}
          onChange={event => this.handleChange(event,'price')}
          margin="none"
         />
       </div>

       <div className="col-12 mt-5">
         <label>{formatMessage(messages.description)}</label>
         <RichTextEditor
                 value={this.state.desc_value}
                 onChange={this.handleEditor}
                 toolbarConfig={toolbarConfig}
               />
        </div>
       <div className="row col-12 mt-5">
         <label className="col-12 mb-3 ">{formatMessage(messages.photo)}</label>


         {
          this.state.filenames && this.state.filenames.map((name,index)=>{
             return <div key={index} className="col-2 m-0   ">
                     <img  className="col-12 p-0   "  src={url+this.state._id+"/"+name}  />
                     <Mutation
                       mutation={DELETE_FILE}
                       onCompleted={(data)=>{
                         if(data.deleteFile===true){
                           let filenames = this.state.filenames;
                           filenames = this.state.filenames.filter(e => e !== name);
                           this.setState({filenames});
                         }

                       }}
                       >
                         {
                           (deleteFile,{data,loading,error})=>{
                             return <button className="btn col-12 btn-light"
                               onClick={e=>{
                                 e.preventDefault();
                                 confirmAlert({
                                    title: formatMessage(messages.deleteConfirm),
                                    message: formatMessage(messages.deleteMsg),
                                    buttons: [
                                      {
                                        label: formatMessage(messages.deleteYes),
                                        onClick: () => deleteFile({variables:{id:this.state._id,name:name}})

                                      },
                                      {
                                        label: formatMessage(messages.deleteNo),
                                        onClick: () => {}
                                      }
                                    ]
                                  })
                               }}
                               ><i className="fa fa-trash"></i></button>
                           }
                         }

                     </Mutation>
                    </div>
           })
          }
        </div>
      <div className="col-4">
        <input
          type="file"
          className="mt-5"
          multiple
          required = { !this.props.edit? 'required' : false}
          onChange={({ target: { validity, files   } }) => //files: [file] for single file
          {
            validity.valid && this.setState({files});
            // console.log(file);
            // console.log(this.props.productId)
            // const productId = this.props.productId; //"3jEJbK9zCo6BBiFd3";//
            // return productId && validity.valid && uploadFile({ variables: { file, productId } })
          }
          }
        />
       </div>


         <button className="btn btn-primary mt-5 mb-5 col-2 offset-10 p-3">
           {!this.props.edit? formatMessage(messages.addBtn) :formatMessage(messages.updateBtn) }
         </button>


       {/* <Snackbar
         anchorOrigin={{ vertical, horizontal }}
         open={open}
         onClose={this.handleClose}
         ContentProps={{
           'aria-describedby': 'message-id',
         }}
         message={<span id="message-id">I love snacks</span>}
       /> */}
     </form>

      </React.Fragment>
   }}
     </Mutation>
     {/* <div className="col-4 mt-3">
      <Upload
        productId={this.state.productId }//this.state.productId}
        uploadDone= {this.handleUploadDone}
        />
      </div> */}
     </React.Fragment>
   );
 }

}

export default injectIntl(NewProduct);
