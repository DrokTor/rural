import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Query } from 'react-apollo';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Link,BrowserRouter,Route } from 'react-router-dom'
import { Button, Col,MDBDatePicker, Container } from 'mdbreact';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TablePagination from '@material-ui/core/TablePagination';
import { injectIntl, defineMessages } from "react-intl";

const messages = defineMessages({
  photo: {
    id: 'product.photo',
    defaultMessage: 'Photo'
  },
  name: {
    id: 'product.name',
    defaultMessage: 'Nom du produit'
  },
  city: {
    id: 'product.city',
    defaultMessage: 'Ville'
  },
  category: {
    id: 'product.category',
    defaultMessage: 'Catégorie'
  },
  family: {
    id: 'product.family',
    defaultMessage: 'Famille'
  },
  weight: {
    id: 'product.weight',
    defaultMessage: 'Poids (kg)'
  },
  price: {
    id: 'product.price',
    defaultMessage: 'Prix (DZ)'
  },
  edit: {
    id: 'product.edit',
    defaultMessage: 'Edit'
  },


})





const GET_PRODUCTS = gql`
  query getProducts($id: [ID], $cat: String, $fam: String, $price: String){
    getProducts(_id: $id,category: $cat,family: $fam,price: $price)  {
      _id
      name
      category
      family
      city
      weight
      price
      availability
      qtyAvailable
      prepTime
      description
      filenames
    }
  }
`;








let id = 0;
function createData(name, calories, fat, carbs, protein) {
  id += 1;
  return { id, name, calories, fat, carbs, protein };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];
class ListProducts extends Component {


constructor(props){
  super(props);

  this.state = {
    order: 'asc',
    orderBy: 'calories',
    selected: [],
    page: 0,
    rowsPerPage: 10,
    open: false,
    vertical: 'bottom',
    horizontal: 'right',
  };

    this.handleChangePage= this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage= this.handleChangeRowsPerPage.bind(this);

}



  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };


 render() {

   const {intl:{formatMessage}} = this.props;

  return (
    <Query
     query={GET_PRODUCTS}
     // gql`
     //   {
     //     getProducts  {
     //       _id
     //       name
     //       city
     //       category
     //       family
     //       weight
     //       price
     //       filenames
     //     }
     //   }
     // `}
     >
     {({ loading, error, data }) => {
       if ( loading) return <div className="mx-auto mt-5 col-3"><CircularProgress   /></div>;
       if (error) return <p>Error :(</p>;
         // console.log(data.getProducts);
         let url = Meteor.absoluteUrl();
         url = url.split('://');
         url[1] = url[1].split(':');
         const baseurl = url[1][0].replace(/\/$/, ""); // remove trailing slash;

         url  = url[0]+"://"+baseurl+':3080/';


      const { order, orderBy, selected, rowsPerPage, page } = this.state;
      const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.getProducts.length - page * rowsPerPage);
      return(
        <Paper className="col-12 mx-auto">
          <Table className="">
            <TableHead>
              <TableRow>
                <TableCell  className="p-0 "> {formatMessage(messages.photo)}</TableCell>
                <TableCell  className="p-0 "> {formatMessage(messages.name)}</TableCell>
                <TableCell  className="p-0 "> {formatMessage(messages.city)}</TableCell>
                <TableCell  className="p-0 "> {formatMessage(messages.category)}</TableCell>
                <TableCell  className="p-0 "> {formatMessage(messages.family)}</TableCell>
                <TableCell  className="p-0 "> {formatMessage(messages.weight)}</TableCell>
                <TableCell  className="p-0 " > {formatMessage(messages.price)}</TableCell>
                <TableCell  className="p-0 " > {formatMessage(messages.edit)}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.getProducts.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {
                //console.log(row);
                return (
                  <TableRow key={row._id}>
                    <TableCell  className="p-0 text-left"  >
                      <img className="p-0 " width="50" src={url+row._id+"/"+(row.filenames && row.filenames[0])}/>
                    </TableCell>
                    <TableCell  className="p-0 text-left" component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell  className="p-0 text-left" numeric>{row.city}</TableCell>
                    <TableCell  className="p-0 text-left" numeric>{row.category}</TableCell>
                    <TableCell  className="p-0 text-left" numeric>{row.family}</TableCell>
                    <TableCell  className="p-0 text-left" numeric>{row.weight}</TableCell>
                    <TableCell  className="p-0 text-left" numeric>{row.price}</TableCell>
                    <TableCell  className="p-0 text-left text-primary" numeric>
                      <Link  to={"/store/products/edit/"+row._id}>
                        <i className="fa fa-pencil-square-o"></i>
                      </Link>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
            <TablePagination
            component="div"
            count={data.getProducts.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
            />
        </Paper>)


         }}
       </Query>
  );


 }

}

export default injectIntl(ListProducts);
