import gql from 'graphql-tag';
import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Query,Mutation } from 'react-apollo';
import CircularProgress from '@material-ui/core/CircularProgress';

export const UPLOAD_MULTIPLE_FILES = gql`
  mutation uploadMultipleFiles($files: [Upload!]!) {
    uploadMultipleFiles(files: $files) {
      filename
    }
  }
`;

export const UPLOAD_FILE = gql`
  mutation uploadFile($file: Upload!, $productId : ID!) {
    uploadFile(file: $file, productId : $productId) {
      filename
      mimetype
      encoding
    }
  }
`;

export default class Upload extends Component  {

  constructor(props){
    super(props);
    this.state={
      file : '',
    }

    this.handleUpload = this.handleUpload.bind(this);
  }

  handleUpload(uploadFile){
      if(this.state.file && this.props.productId){
        const file = this.state.file;
        const productId = this.props.productId;
          uploadFile({ variables: { file, productId } });
      }
  }

  render(){
    // console.log(this.props.uploadDone);
     console.log(this.props.productId)
      return (
        <Mutation
          mutation={UPLOAD_MULTIPLE_FILES}
        //  onCompleted={data => {  alert('done'); this.props.uploadDone()} }
          ///onCompleted={ ()=> alert('comp') }
          onError={(error) =>console.log(error)}
          >

          {(uploadFile, { loading, error,data }) => {

          if(loading){
            return <div className="mx-auto mt-5 col-3"><CircularProgress   /></div>;
          }
           console.log(error);
           //console.log(this.props.productId);
           //this.handleUpload(uploadFile);
            return <input
              type="file"
              multiple
              required
              onChange={({ target: { validity, files  } }) => //files:: [file] for single file
              {
                validity.valid && uploadFile({ variables: { files } });
                // console.log(file);
                // console.log(this.props.productId)
                // const productId = this.props.productId; //"3jEJbK9zCo6BBiFd3";//
                // return productId && validity.valid && uploadFile({ variables: { file, productId } })
              }
            }
          />
        }}
      </Mutation>
    );
  }

};
