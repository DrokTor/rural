import React, { Component } from 'react';
import { Query } from "react-apollo";
import gql from "graphql-tag";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
//import Icon from '@material-ui/core/Icon';
import {Link} from "react-router-dom";
import { injectIntl, defineMessages } from "react-intl";

//import {DeleteIcon} from '@material-ui/icons/DeleteIcon';
import {Delete , ShoppingCart, AddShoppingCart, RemoveShoppingCart} from '@material-ui/icons';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';
import Rating from 'react-rating';

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

const cat_list=[
  {
    name: "Bijoux",
    name_tr: "jewelry",
    family: [{name:"Coliers",name_tr:"necklaces",id_tr: 'menu.content1_1'},{name:"Bracelets",name_tr:"braclets",id_tr: 'menu.content1_2'},{name:"Anneaux",name_tr:"rings",id_tr: 'menu.content1_3'},{name:"Boucles d'oreilles",name_tr:"earrings",id_tr: 'menu.content1_4'}]
  },
  {
    name: "Vêtements",
    name_tr: "clothings",
    family: [{name:"Écharpes",name_tr:"scarves",id_tr: 'menu.content2_1'},{name:"chemises",name_tr:"shirts",id_tr: 'menu.content2_2'},{name:"Gants",name_tr:"gloves",id_tr: 'menu.content2_3'}]
  },
  {
    name: "Maison",
    name_tr: "home",
    family: [{name:"Tapis",name_tr:"rugs",id_tr: 'menu.content3_1'},{name:"Cadres",name_tr:"paintings",id_tr: 'menu.content3_2'},{name:"Vases",name_tr:"vases",id_tr: 'menu.content3_3'}]
  },
  {
    name: "Ustensiles",
    name_tr: "utensils",
    family: [{name:"Tasses",name_tr:"mugs",id_tr:"menu.content4_1"},{name:"Plats",name_tr:"dishes",id_tr:"menu.content4_2"},{name:"Cuillères",name_tr:"spoons",id_tr:"menu.content4_3"}]
  },

];



let messages = defineMessages({
  content1: {
    id: 'products.content1',
    defaultMessage: 'résultat.'
  },
  content2_title: {
    id: 'products.content2_title',
    defaultMessage: 'Catégorie'
  },
  content2_1: {
    id: 'menu.content1.title',
    defaultMessage: 'Bijoux'
  },
  content2_2: {
    id: 'menu.content2.title',
    defaultMessage: 'Vêtements'
  },
  content2_3: {
    id: 'menu.content3.title',
    defaultMessage: 'Maison'
  },
  content2_4: {
    id: 'menu.content4.title',
    defaultMessage: 'Ustensiles'
  },
  content3_title: {
    id: 'products.content3_title',
    defaultMessage: 'Famille'
  },
  content4_title: {
    id: 'products.content4_title',
    defaultMessage: 'Prix'
  },
  content5_title: {
    id: 'products.content5_title',
    defaultMessage: 'Filtrer'
  },
  content6_title: {
    id: 'products.content6_title',
    defaultMessage: 'Disponibilité'
  },
  content6_1: {
    id: 'products.content6_1',
    defaultMessage: 'Disponible'
  },
  content6_2: {
    id: 'products.content6_2',
    defaultMessage: 'Sur commande'
  },
  wishListEmpty: {
    id: 'products.wishListEmpty',
    defaultMessage: 'La liste des produits souhaité est vide.'
  },
})

cat_list.map((cat,index)=>{
  cat.family.map((fam,ind)=>{
    messages[cat.name+'_fam_'+ind]={
      id: fam.id_tr,
      defaultMessage: fam.name
      }
  })
})
//console.log(messages);
//console.log(cat_list.find(x=>x.name=== "Ustensiles"));
class Products extends Component {

  constructor(props){
    super(props);

    this.state={
      category: '',
      family: '',
      price: [0,1000],
    }
    //
    // this.handleWish = this.handleWish.bind(this);
  }
  //
  // handleWish(elem){
  //
  //     let wishList = this.state.wishList;
  //     //const elem = event.target.dataset.id;
  //     if(wishList.indexOf(elem) === -1){
  //       wishList.push(elem);
  //       console.log('aded');
  //       console.log(elem);
  //     }else {
  //       wishList.splice(wishList.indexOf(elem), 1);
  //       console.log('removed');
  //     }
  //     this.setState({wishList},()=>{
  //
  //        // call handle with from parent too to update header
  //     });
  // }




  render(){
    // let url = window.location.href.split('/category/')[1] ;
    // const [category,family] = url? url.split('/'):""  ;
    // console.log(category+" "+family);
    const {intl:{formatMessage}} = this.props;
    const urlParams = this.props.match.params;
    //console.log(this.props.match.params.cat+" "+this.props.match.params.fam);


    const GET_WISH_SHOPLIST = gql`
    {
      wishList @client
      shopList @client{
        _id
        qty
      }
      cost     @client
    }
    `;
    const GET_PRODUCTS = gql`
      query getProducts($id: [ID], $cat: String, $fam: String,$avail: String, $price: String){
        getProducts(_id: $id,category: $cat,family: $fam, availability: $avail, price: $price)  {
          _id
          name
          category
          family
          price
          filenames
          rating
        }
      }
    `;

    let variables = {};
    if(!this.props.showShopList && this.props.showWishList && this.props.showWishList.length>0){
      variables.id  = this.props.showWishList ;
    }
    // else if(!this.props.showWishList && this.props.showShopList &&  this.props.showShopList.length>0){
    //   variables.id  = this.props.showShopList ;
    // }
    urlParams.cat? variables.cat = urlParams.cat : '' ;
    urlParams.fam? variables.fam = urlParams.fam : '' ;
    urlParams.avail? variables.avail = urlParams.avail : '' ;
    urlParams.price? variables.price = urlParams.price : '' ;
    //console.log(variables);
    if(!this.props.showShopList && this.props.showWishList && this.props.showWishList.length===0){
      return <div className="min-height-500"><div className="col-4 mx-auto alert-warning mt-5 p-5 text-center">{formatMessage(messages.wishListEmpty)}</div></div>;
    }

    return <div className="row col-12 min-height-500">
  {  !this.props.showWishList &&
     <div className=" col-3 mt-5">
        {/* <label>configurator</label> */}
        <form>
        <label>{formatMessage(messages.content2_title)}</label>
        <select className="bg-white mb-4 custom-select col-12" value={this.state.category} onChange={e=>this.setState({category:e.target.value})}>
            <option value=""></option>
            <option value="jewelry">{formatMessage(messages.content2_1)}</option>
            <option value="clothings">{formatMessage(messages.content2_2)}</option>
            <option value="home">{formatMessage(messages.content2_3)}</option>
            <option value="utensils">{formatMessage(messages.content2_4)}</option>
        </select>
        <label>{formatMessage(messages.content3_title)}</label>
        <select className="bg-white mb-4 custom-select col-12" value={this.state.family} onChange={e=>this.setState({family:e.target.value})}>
                <option value=""></option>
            { (()=>{
                let cat = cat_list.find(x=>x.name_tr === this.state.category);
                // console.log(cat);
                // console.log(this.state.category);
                if(cat){
                  return cat.family.map((fam,index)=>{
                    return <option key={index} value={fam.name_tr}>{formatMessage(messages[cat.name+'_fam_'+index])}</option>
                  })
                }
              })()
            }
        </select>
        <label>{formatMessage(messages.content6_title)}</label>
        <select className="bg-white mb-4 custom-select col-12" value={this.state.availability} onChange={e=>this.setState({availability:e.target.value})}>
                <option value=""></option>
                <option value="available">{formatMessage(messages.content6_1)}</option>
                <option value="by order">{formatMessage(messages.content6_2)}</option>
        </select>
        <label>{formatMessage(messages.content4_title)}</label>
        <div className="col-12">
          {/* <Slider min={20} defaultValue={20} marks={{ 20: 20, 40: 40, 100: 100 }} step={null} /> */}
          <Range
            min={0}
            max={1000}
            defaultValue={this.state.price}
            tipFormatter={value => `${value}`}
            onChange={ data => this.setState({price: data}) }
          />
        <div className="row col-12">
          <label className="col-6 text-center"> {">= "+this.state.price[0]}</label>
          <label className="col-6 text-center"> {"<= "+this.state.price[1]}</label>
        </div>


        </div>
        <Link className="btn bg-beige p-3 col-6 offset-3 mt-5 " to={"/products/"+
        (this.state.category? this.state.category+"/":null+"/")+
        (this.state.family? this.state.family+"/":"null/")+
        (this.state.availability? this.state.availability+"/":"null/")+
        (this.state.price? this.state.price.join('-')+"/":"")
      }>
          <i className="fa fa-filter"></i> {formatMessage(messages.content5_title)}
        </Link>
        </form>
      </div>
      }
      <div className="row col-9 mx-auto mt-5 pt-3">
      {
        this.props.showShopList && <div className="row col-12 mb-5 ">
          <div className="row col-9 p-4 border">
            <div className="col-3">Total:</div>
            <label className="col-9">{this.props.cost+" € "}</label>
          </div>
          <button className="btn btn-warning col-3   p-3 pb-4" onClick={this.props.handleOrder}>COMMMANDER <i className="fa fa-cart-arrow-down fa-2x text-white"></i></button>
          </div>
      }
     <Query
      query={GET_PRODUCTS}
      variables={variables}
      fetchPolicy={"cache-and-network"}
      //variables={{id : ["CJ2gth3WxgyQx2DCv"]  }}
      //pollInterval={2000}
      >
      {({ loading, error, data, variables }) => {
        // console.log(variables);
        if (loading) return <p>Loading...</p>;
        if (error) return <p>Error :(</p>;
        //random image remove for product
        let url = Meteor.absoluteUrl();
        url = url.split('://');
        url[1] = url[1].split(':');
        const baseurl = url[1][0].replace(/\/$/, ""); // remove trailing slash;

        url  = url[0]+"://"+baseurl+':3080/';

        //console.log(data);
        if(data.getProducts.length==0){
          return <div>0 {formatMessage(messages.content1)}</div>
        }
        return data.getProducts.map((product,index) => {
          //let img= "/assets/images/"+Math.floor((Math.random() * 4) + 1)+".jpeg";
        //  console.log( (product.rating));
    return <div key= {index} className="col-3 mb-5 ">
            <Link to={"/product/"+product._id} className="no-deco">
            <div className="row col-12 m-0 p-0 pointer border" >
              {/* <div className="position-relative image-container d-flex align-items-center"> */}
              <div className="position-relative image-container2 d-flex align-items-center">
                <img className="p-0 col-12 " src={url+product._id+"/"+product.filenames[0]}/>
              <div  className="col-12   heart-container2">

                {/* <i data-id={product._id} className={"float-right fa   fa-wish pointer  "+(this.props.wishList.indexOf(product._id)=== -1 ? " fa-heart-o":" fa-heart wish-color" )} onClick={event => {event.preventDefault(); this.props.handleWish(event.target.dataset.id)}}></i> */}
                <Query query={GET_WISH_SHOPLIST}>
                  {({ data, client }) => {


                    if(!data.wishList){
                      client.writeData({ data:  {wishList:[]}  });
                    }


                    return(
                    // <i data-id={product._id}
                    //    className={"float-right fa   fa-wish pointer  "+(data.wishList && data.wishList.indexOf(product._id) != -1 ? " fa-heart wish-color":" fa-heart-o " )}
                    //    onClick={e =>{ e.preventDefault(); this.props.handleWish(client,data,product._id) }}
                    //    ></i>
                       <div className="float-right d-flex align-items-center justify-content-center">
                       <i data-id={product._id}
                          className={" fa   fa-wish pointer  "+(data.wishList && data.wishList.indexOf(product._id) != -1 ? " fa-heart wish-color":" fa-heart-o " )}
                        onClick={e =>{
                          e.preventDefault();
                           this.props.handleWish(client,data,product._id) }}
                        ></i>
                        </div>)
                  }}
                </Query>
                {/* <i data-id={product._id} className= {"col text-right fa  fa-2x pointer float-right"+(this.props.shopList.indexOf(product._id)=== -1? " fa-shopping-cart ":" fa-cart-arrow-down cart-color")}  onClick={event => {event.preventDefault(); this.props.handleShop(event.target.dataset.id,product.price)}}></i> */}

                {/* {
                  this.props.shopList.indexOf(product._id)=== -1?
                  <i data-id={product._id} onClick={event => this.props.handleShop(event.target.dataset.id)}><AddShoppingCart  className="" fontSize="large"   /></i>
                  :
                  <i data-id={product._id} onClick={event => this.props.handleShop(event.target.dataset.id)}><RemoveShoppingCart  className="" fontSize="large"  /></i>
                } */}



              </div>
              </div>
              <div className="col-12 pt-2 pb-2 ">{product.name}</div>
              <Rating
                className="col-12 "
                emptySymbol="fa fa-star-o fs-0-8"
                fullSymbol="fa fa-star fs-0-8"
                initialRating={parseFloat(product.rating)}
                readonly
               />
              <div className="col-8 pt-2 pb-2 font-weight-bold ">
                {product.price}
              </div>
              <Query query={GET_WISH_SHOPLIST}>
                {({ data, client }) => {
                  // if(!data.shopList){
                  //   client.writeData({ data:  {shopList:[],cost:0}  });
                  // }
                  //console.log(data);
            return  <div className="mini-cart col-4 pt-2 pb-2 font-weight-bold text-right">
                <i  className= {"  fa   fs-1-5 pointer   "+(data.shopList && data.shopList.findIndex(x=>x._id===product._id) != -1 ? " fa-cart-arrow-down text-coral":" fa-shopping-cart " )}
                  onClick={e =>{
                    e.preventDefault();
                    //client.writeData({data: {shopList: {_id:"x2sddoijkdl",qty:"2",__typename:"Item_qty"} }})
                    this.props.handleShop(client,data,product._id,product.price)
                    }}
                     ></i>
              </div>
              }}
            </Query>
            </div>
            </Link>

            {/* { (this.props.shopList.indexOf(product._id)=== -1)?
              <button
              data-id={product._id}
              className= " col-12 btn  add-to-cart  pl-0 pr-0"
              onClick={event => this.props.handleShop(event.target.dataset.id, product.price)}>
              <div className="row m-0 d-flex align-items-center">
                <div className="col-8">Ajouter au panier</div>
                <div className="col-4">
                  <i  className=  "col-12  pt-0 pb-0 fa  fa-2x pointer  fa-shopping-cart "   ></i>
                </div>
              </div>
            </button>
            :
            <button
              data-id={product._id}
              className= " col-12 btn  btn-outline-success rounded-0 pl-0 pr-0"
              onClick={event => this.props.handleShop(event.target.dataset.id, product.price)}>
              <div className="row m-0 d-flex align-items-center">
                <div className="col-8">Dans le panier</div>
                <div className="col-4">
                  <i  className=  "col-12  pt-0 pb-0 fa  fa-2x pointer  fa-check"   ></i>
                </div>
              </div>
            </button> */}

          </div>

        });
      }}
    </Query>
    </div>
  </div>

  }

}


export default injectIntl(Products);
