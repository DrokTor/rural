import React, { Component } from 'react';
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
//import Icon from '@material-ui/core/Icon';
import {Link} from "react-router-dom";
import ReactImageMagnify from 'react-image-magnify';
import Rating from 'react-rating';
import { injectIntl, defineMessages } from "react-intl";
//import {DeleteIcon} from '@material-ui/icons/DeleteIcon';
import {Delete , ShoppingCart, AddShoppingCart, RemoveShoppingCart} from '@material-ui/icons';

const messages = defineMessages({

  content1: {
    id: 'showProduct.content1',
    defaultMessage: 'Ajouter au panier'
  },
  content2: {
    id: 'showProduct.content2',
    defaultMessage: "Visiter ma boutique"
  },
  content3: {
    id: 'showProduct.content3',
    defaultMessage: "Plus"
  },
  content4: {
    id: 'showProduct.content4',
    defaultMessage: "Ajouter un avis"
  },
  content5: {
    id: 'showProduct.content5',
    defaultMessage: "Titre"
  },
  content6: {
    id: 'showProduct.content6',
    defaultMessage: "Commentaire"
  },
  content7: {
    id: 'showProduct.content7',
    defaultMessage: "Note"
  },
  content8: {
    id: 'showProduct.content8',
    defaultMessage: "Envoyer"
  },
  content9: {
    id: 'showProduct.content9',
    defaultMessage: "Vous devez être connecter pour poster un avis."
  },
  content10: {
    id: 'showProduct.content10',
    defaultMessage: "Avis rajouté avec succès."
  },

})

class ShowProduct extends Component {

  constructor(props){
    super(props);

    this.state={
      image: this.props.imageState || 0,
    }

    this.handleImage = this.handleImage.bind(this);
    this.htmlUnescape = this.htmlUnescape.bind(this);
  }

  handleImage(elem){
    //this

  }
  htmlUnescape(str){

      return !str? '': str
          .replace(/&quot;/g, '"')
          .replace(/&#39;/g, "'")
          .replace(/&lt;/g, '<')
          .replace(/&gt;/g, '>')
          .replace(/&amp;/g, '&') ;
  }


  render(){
    const {intl:{formatMessage}} = this.props;
    //get id from url
    // let id = window.location.href.split('/product/')[1] ;
    // id = id.split('#')[0] ;
    const id = this.props.match.params.id ;
    //console.log(id);
    const GET_PRODUCTS = gql`
      query getProducts($id: [ID]){
        getProducts(_id: $id)  {
          _id
          name
          category
          family
          availability
          qtyAvailable
          prepTime
          price
          filenames
          rating
          description
          ratings{
            _id
            product_id
            rating
            title
            comment
          }
        }
      }
    `;

    const GET_WISH_SHOPLIST = gql`
    {
      wishList @client
      shopList @client {
        _id
        qty
      }
      cost  @client
    }
    `;

    const ADD_RATING = gql`
      mutation AddRating($product_id: ID!, $rating: Int!, $title: String!, $comment: String!) {
        addRating(product_id: $product_id, rating:$rating, title:$title,comment:$comment){
          product_id
          rating
          title
          comment
        }
      }
    `;
    const variables = {id: [id]};

  return  <Query
      query={GET_PRODUCTS}
      variables={variables}
      fetchPolicy={"cache-and-network"}
      //variables={{id : ["CJ2gth3WxgyQx2DCv"]  }}
      //pollInterval={2000}
        >
      {({ loading, error, data, variables }) => {
        // console.log(variables);
        if (loading) return <p>Loading...</p>;
        if (error) return <p>Error :(</p>;

         //console.log(data);
        const product = data.getProducts[0];
        //let img= "/assets/images/"+Math.floor((Math.random() * 4) + 1)+".jpeg";
        let url = Meteor.absoluteUrl();
        url = url.split('://');
        url[1] = url[1].split(':');
        const baseurl = url[1][0].replace(/\/$/, ""); // remove trailing slash;

        url  = url[0]+"://"+baseurl+':3080/';

  return   <div className="row col-8 mx-auto mt-5 pt-5">
              <div className="col-2">
                {
                  product.filenames.map((name,index)=>{
                    return <img key={index} className="pointer col-6 m-0 p-1  " src={url+product._id+"/"+name} onClick={()=>this.props.handleImage( index )}/>
                  })
                }

              </div>
              <div className="col-6 border">
                {/* <img className="row mx-auto image-show" src={url+product._id+"/"+product.filenames[this.state.image]}/> */}
                <ReactImageMagnify {...{
                    className: "row mx-auto image-show",
                    smallImage: {
                        //alt: 'Wristwatch by Ted Baker London',
                        //isFluidWidth: true,
                        width: 300,
                        height: 300,
                        src: url+product._id+"/"+product.filenames[this.props.imageState],
                    },
                    largeImage: {
                        src: url+product._id+"/"+product.filenames[this.props.imageState],
                        width: 1200,
                        height: 1200,
                    },
                    imageClassName: 'img-show-cls',
                    enlargedImagePosition : 'over',
                    isHintEnabled: true,
                    shouldHideHintAfterFirstActivation: true,
                }} />
              </div>
              <div className="col-4 pl-5">
                <div className="col-12 pt-2 pb-2 font-weight-bold">{product.name}</div>
                <div className="col-12 pt-2 pb-2 ">{product.category}</div>
                <div className="col-12 pt-2 pb-2 ">{product.family}</div>
                <div className="col-12 pt-2 pb-2 font-weight-bold">{product.availability}</div>
                <div className="col-12 pt-2 pb-2 ">({product.availability=="by order"? product.prepTime+" days to be produced" : product.qtyAvailable+" remaining"})</div>
                <div className="col-12 pt-2 pb-2 ">{product.town}</div>
                <div className="col-12 pt-2 pb-2 ">{product.weight}</div>
                <div className="col-12 pt-2 pb-2 font-weight-bold">{product.price}</div>
                <div className="col-12">
                <Rating
                  emptySymbol="fa fa-star-o "
                  fullSymbol="fa fa-star "
                  initialRating={parseFloat(product.rating)}
                  readonly
                  />
                </div>
                {/* <div  className="row col-12 mt-3 align-items-center">
                  <i data-id={product._id} className={"col-6  pt-2 pb-2 fa   fa-2x pointer  "+(this.props.wishList.indexOf(product._id)=== -1 ? " fa-heart-o":" fa-heart wish-color" )} onClick={event => this.props.handleWish(event.target.dataset.id)}></i>
                </div> */}
                <Query query={GET_WISH_SHOPLIST}>
                  {({ data, client }) => {
return(
                    <i data-id={product._id}
                       className={"col-6  pt-2 pb-2 fa   fa-2x pointer  "+(data.wishList && data.wishList.indexOf(product._id) != -1 ? " fa-heart wish-color":" fa-heart-o " )}
                       onClick={e =>{
                         e.preventDefault();
                          this.props.handleWish(client,data,product._id) }}
                       ></i>
                     )
                  }}
                </Query>
                <Query query={GET_WISH_SHOPLIST}>
                  {({ data, client }) => {
  // return            <div className="mini-cart col-4 pt-2 pb-2 font-weight-bold text-right">
  //                   <i  className= {"  fa   fs-1-5 pointer   "+(data.shopList && data.shopList.findIndex(x=>x._id===product._id) != -1 ? " fa-cart-arrow-down text-coral":" fa-shopping-cart " )}
  //                     onClick={e =>{
  //                       e.preventDefault();
  //                       //client.writeData({data: {shopList: {_id:"x2sddoijkdl",qty:"2",__typename:"Item_qty"} }})
  //                       this.props.handleShop(client,data,product._id,product.price)
  //                       }}
  //                        ></i>
  //                   </div>
              return  <div className="row col-12">
                  { (data.shopList && data.shopList.findIndex(x=>x._id===product._id) === -1)?
                    <button
                    data-id={product._id}
                    className= " col-12 btn  add-to-cart "
                    onClick={event => this.props.handleShop(client,data,product._id,product.price)}>
                    <div className="row m-0 d-flex align-items-center">
                      <div className="col-8">{formatMessage(messages.content1)}</div>
                      <div className="col-4">
                        <i  className=  "col-12  pt-2 pb-2 fa  fa-2x pointer  fa-shopping-cart "   ></i>
                      </div>
                    </div>
                  </button>
                  :
                  <button
                    data-id={product._id}
                    className= " col-12 btn  btn-outline-success "
                    onClick={event => this.props.handleShop(client,data,product._id,product.price)}>
                    <div className="row m-0 d-flex align-items-center">
                      <div className="col-8">Dans le panier</div>
                      <div className="col-4">
                        <i  className=  "col-12  pt-2 pb-2 fa  fa-2x pointer  fa-check"   ></i>
                      </div>
                    </div>
                  </button>
                  }
                </div>
              }}

            </Query>
              </div>
              <div className="row col-8 mt-3 mb-3 pt-2 pb-2 ">
                <label className="row col-12" role="button" data-toggle="collapse" data-target="#prod-desc">Description</label>
                <div

                  className={"  "+(!this.state.descLimit ? "desc-limit":"")}
                  dangerouslySetInnerHTML={{__html : this.htmlUnescape(product.description)}}>

                </div>
                <div className="col-12 pt-3">
                  <div className="col-12 border-top pointer" onClick={()=>this.setState({descLimit:!this.state.descLimit})}>+ {formatMessage(messages.content3)}</div>
                </div>
              </div>
              <div className="col-3 mt-5 offset-1 p-4 bg-beige">
                {formatMessage(messages.content2)}
              </div>
              <div className=" col-12 mt-5">
                <div className=" col-12">
                  <Mutation
                    mutation={ADD_RATING}
                    // onCompleted={ data => this.handleAdd() }
                    update={(cache, { data: { addRating } }) => {
                       //console.log(addRating);
                       //console.log(cache.data.data.ROOT_QUERY);
                       //console.log(cache.data.data.ROOT_QUERY.getProducts);
                            //console.log(cache.data.data.ROOT_QUERY);
                             const products  = cache.readQuery({ query: GET_PRODUCTS, variables });

                            //console.log(products);
                            // console.log(products.getProducts[0].ratings.concat([addRating]));
                            //console.log(products.getProducts[0].ratings.push(addRating));
                            products.getProducts[0].ratings.unshift(addRating);
                            //console.log(variables.id[0]);
                            // cache.writeQuery({
                            //   query: GET_PRODUCTS,
                            //   data: { getProducts :  products }
                            // });
                            // if(cache.data.data.ROOT_QUERY.getProducts){
                            // }
                     }}
                    >
                    {(addRating,{data,loading,error})=>{
                if(this.state.ratingAdding)
                return <div className="alert alert-success">
                  {formatMessage(messages.content10)}
                </div>

          return  <React.Fragment>
                  <button className="btn btn-outline-brown mt-5  mb-3"
                    data-toggle="collapse" data-target="#rate-section"
                    >
                    <i className="fa fa-arrow-down"></i> {formatMessage(messages.content4)}
                  </button>
                  <div id="rate-section" className="row col-12 p-0 m-0  collapse">
                    {
                      !Meteor.userId()?
                      <div className="alert alert-warning">
                          {formatMessage(messages.content9)}
                      </div>
                      :
                      <React.Fragment>
                        <label className="col-8 p-2 ">{formatMessage(messages.content5)}</label>
                        <label className="col-3 offset-1 p-2 pl-4">{formatMessage(messages.content7)}</label>
                        <input
                          className="col-8"
                          type="text"
                          onChange={event=>this.setState({ratingTitle:event.target.value})}
                        />
                        <Rating
                          className="col-3 offset-1 "
                          emptySymbol="fa fa-star-o fa-2x"
                          fullSymbol="fa fa-star fa-2x"
                          onChange={value=>this.setState({ratingValue:value})}
                          initialRating={parseFloat(this.state.ratingValue)}
                        />
                        <label  className=" mt-2 p-2">{formatMessage(messages.content6)}</label>
                        <textarea
                          className="col-12 border"
                          onChange={event=>this.setState({ratingComment:event.target.value})}

                        />
                        <button
                          className="btn btn-brown mt-3 col-3 offset-9"
                          onClick={()=>{//console.log(product._id);
                            addRating({variables : {product_id:product._id,rating:this.state.ratingValue,title:this.state.ratingTitle,comment:this.state.ratingComment}})
                            this.setState({ratingAdding:true});
                          }}
                          >
                          <i className="fa fa-plus"></i> {formatMessage(messages.content8)}
                        </button>
                      </React.Fragment>
                    }

                  </div>
                  </React.Fragment>
                  }}
                  </Mutation>
                </div>

                <div className="mt-5 ">
                  {
                    product.ratings.map((elem,index)=>{
                      //console.log(elem);
                      return <div
                        id={elem._id}
                        key={index}
                        className="row mb-3 mt-3 border border-bottom-brown-2 p-4  ">
                        {/* <label>Note</label> */}
                        <Rating
                          className="col-9"
                          emptySymbol="fa fa-star-o "
                          fullSymbol="fa fa-star "
                          initialRating={parseFloat(elem.rating)}
                          readonly
                        />
                        <div className=" col-3 text-right">
                          {moment(elem.date).format('DD-MM-YYYY')}
                        </div>
                        {/* <label>Titre</label> */}
                        <div className="mt-1 col-12 font-weight-bold">{elem.title}</div>
                        {/* <label>Commentaire</label> */}
                        <div className="mt-1 col-12 ">{elem.comment}</div>
                      </div>
                    })
                  }
                </div>
              </div>
            </div>



    }}
    </Query>


  }

}
export default injectIntl(ShowProduct)
