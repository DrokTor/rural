import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { injectIntl, defineMessages } from "react-intl";


const messages = defineMessages({

  content1_title: {
    id: 'menu.content1.title',
    defaultMessage: 'Bijoux'
  },
  content1_1: {
    id: 'menu.content1_1',
    defaultMessage: "Coliers"
  },
  content1_2: {
    id: 'menu.content1_2',
    defaultMessage: "Bracelets"
  },
  content1_3: {
    id: 'menu.content1_3',
    defaultMessage: "Anneaux"
  },
  content1_4: {
    id: 'menu.content1_4',
    defaultMessage: "Boucles d'oreilles"
  },
  content_all: {
    id: 'menu.content_all',
    defaultMessage: "Tous"
  },
  content2_title: {
    id: 'menu.content2.title',
    defaultMessage: 'Vêtements'
  },
  content2_1: {
    id: 'menu.content2_1',
    defaultMessage: "Écharpes"
  },
  content2_2: {
    id: 'menu.content2_2',
    defaultMessage: "chemises"
  },
  content2_3: {
    id: 'menu.content2_3',
    defaultMessage: "Gants"
  },
  content3_title: {
    id: 'menu.content3.title',
    defaultMessage: 'Maison'
  },
  content3_1: {
    id: 'menu.content3_1',
    defaultMessage: "Tapis"
  },
  content3_2: {
    id: 'menu.content3_2',
    defaultMessage: "Cadres"
  },
  content3_3: {
    id: 'menu.content3_3',
    defaultMessage: "Vases"
  },
  content4_title: {
    id: 'menu.content4.title',
    defaultMessage: 'Ustensiles'
  },
  content4_1: {
    id: 'menu.content4_1',
    defaultMessage: "Tasses"
  },
  content4_2: {
    id: 'menu.content4_2',
    defaultMessage: "Plats"
  },
  content4_3: {
    id: 'menu.content4_3',
    defaultMessage: "Cuillères"
  },

})



// App component - represents the whole app

class Menu extends Component {

  render() {
    const {intl:{formatMessage}} = this.props;

    return (

      <div className="row   bg-light menu-container ">
        <nav className="  navbar navbar-expand-lg col-8 mx-auto  p-0 ">
           <ul className="navbar-nav col-3 ">
             <li className="nav-item dropdown">
               <span className="nav-link pointer dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <a href="#">{formatMessage(messages.content1_title)}</a>
               </span>
               <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                 <Link to="/products/jewelry/necklaces" className="dropdown-item"> {formatMessage(messages.content1_1)} </Link>
                 <Link to="/products/jewelry/braclets" className="dropdown-item"> {formatMessage(messages.content1_2)} </Link>
                 <Link to="/products/jewelry/rings" className="dropdown-item"> {formatMessage(messages.content1_3)} </Link>
                 <Link to="/products/jewelry/earrings" className="dropdown-item"> {formatMessage(messages.content1_4)} </Link>
                 <Link to="/products/jewelry/" className="dropdown-item">  <i className="fa fa-angle-right"></i> {formatMessage(messages.content_all)}</Link>
               </div>
             </li>
           </ul>
           <ul className="navbar-nav col-3 ">
             <li className="nav-item dropdown">
               <span className="nav-link pointer dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <a href="#">{formatMessage(messages.content2_title)}</a>
               </span>
               <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                 <Link to="/products/clothings/scarves" className="dropdown-item">  {formatMessage(messages.content2_1)}  </Link>
                 <Link to="/products/clothings/shirts" className="dropdown-item">  {formatMessage(messages.content2_2)} </Link>
                 <Link to="/products/clothings/gloves" className="dropdown-item"> {formatMessage(messages.content2_3)}  </Link>
                 <Link to="/products/clothings/" className="dropdown-item"> <i className="fa fa-angle-right"></i> {formatMessage(messages.content_all)}</Link>
               </div>
             </li>
           </ul>
           <ul className="navbar-nav col-3 ">
             <li className="nav-item dropdown">
               <span className="nav-link pointer dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <a href="#">{formatMessage(messages.content3_title)}</a>
               </span>
               <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                 <Link to="/products/home/rugs" className="dropdown-item"> {formatMessage(messages.content3_1)} </Link>
                 <Link to="/products/home/frames" className="dropdown-item"> {formatMessage(messages.content3_2)} </Link>
                 <Link to="/products/home/vases" className="dropdown-item"> {formatMessage(messages.content3_3)} </Link>
                 <Link to="/products/home/" className="dropdown-item"> <i className="fa fa-angle-right"></i>  {formatMessage(messages.content_all)}</Link>
               </div>
             </li>
           </ul>
           <ul className="navbar-nav col-3 ">
             <li className="nav-item dropdown">
               <span className="nav-link pointer dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <a href="#">{formatMessage(messages.content4_title)}</a>
               </span>
               <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                 <Link to="/products/utensils/mugs" className="dropdown-item"> {formatMessage(messages.content4_1)} </Link>
                 <Link to="/products/utensils/dishes" className="dropdown-item"> {formatMessage(messages.content4_2)}  </Link>
                 <Link to="/products/utensils/spoons" className="dropdown-item"> {formatMessage(messages.content4_3)}  </Link>
                 <Link to="/products/utensils/" className="dropdown-item"> <i className="fa fa-angle-right"></i>  {formatMessage(messages.content_all)}</Link>
               </div>
             </li>
           </ul>
         </nav>
      </div>
    );

  }

}

export default injectIntl(Menu);
