import React, { Component } from 'react';
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
import { _ } from 'lodash';
//import Icon from '@material-ui/core/Icon';
import {Link} from "react-router-dom";
//import {DeleteIcon} from '@material-ui/icons/DeleteIcon';
import {Delete , ShoppingCart, AddShoppingCart, RemoveShoppingCart} from '@material-ui/icons';
import { injectIntl, defineMessages } from "react-intl";


const messages = defineMessages({
  content1: {
    id: 'purchases.content1',
    defaultMessage: 'Achats'
  },
  content2: {
    id: 'purchases.content2',
    defaultMessage: 'Vous n\'avez aucun produit dans votre panier.',
  },
  quantity: {
    id: 'orders.content2',
    defaultMessage: 'Quantité',
  },
  price: {
    id: 'product.price',
    defaultMessage: 'Prix',
  },
  envoyée: {
    id: 'state.sent',
    defaultMessage: 'envoyée'
  },
  commande: {
    id: 'state.order',
    defaultMessage: 'commande'
  },
  validée: {
    id: 'state.accepted',
    defaultMessage: 'validée'
  },
  reçue: {
    id: 'state.received',
    defaultMessage: 'reçue'
  },
  clôturée: {
    id: 'state.closed',
    defaultMessage: 'clôturée'
  },
  order: {
    id: 'purchases.order',
    defaultMessage: 'Commande'
  },
  productsCount: {
    id: 'purchases.productsCount',
    defaultMessage: 'Produits'
  },
  total: {
    id: 'purchases.total',
    defaultMessage: 'Total'
  },




});




class Purchases extends Component {

  constructor(props){
    super(props);

    this.state={
      order: false,
      state: '',
    }

    // this.handleWish = this.handleWish.bind(this);
  }




  render(){
    const {intl:{formatMessage}} = this.props;

    const GET_ORDERS = gql`
      query getMyOrders($id: ID, $buyer: Boolean = true, $type:String = "buyer",  $state: String){
        getMyOrders(_id: $id, buyer: $buyer)  {
          _id
          buyer_id
          date
          cost
          items(order_id: $id, type:$type, state : $state)
            {
              _id,
              seller_id,
              order_id,
              product_id,
              name,
              img,
              buyer_id,
              date,
              qty,
              cost,
              state,
              error,
            }
        }
      }
    `;

    const GET_ITEMS = gql`
      query getItems($id: ID, $type:String = "buyer",  $state: String ){
        getItems(order_id: $id, type:$type, state : $state  )  {
          _id,
          seller_id,
          order_id,
          product_id,
          name,
          img,
          buyer_id,
          date,
          qty,
          state,
          error,
        }
      }
    `;

    const SET_ORDER = gql`
      mutation SetOrderItem($id: ID, $state: String! ) {
        setOrderItem(_id:$id, state:$state ){
          _id
          buyer_id
          date
          product_id
          state
        }
      }
    `;

    let variables = {};
    if( this.state.order ){
      variables.id  = this.state.order ;
    }
    if( this.state.state ){
      variables.state  = this.state.state ;
    }

    // console.log(this.props.whishList);
    // if( this.props.showShopList &&  this.props.showShopList.length===0){
    //   return <div className="col-4 mx-auto alert-warning mt-5 p-5 text-center">{formatMessage(messages.content2)}</div>
    // }

    return <div className=" col-8 mx-auto  mt-3 min-height-500">
      <div className="row col-12 mb-5 ">
        <h3 className="col-3">{formatMessage(messages.content1)}</h3>
        <div className="row col-9">
          <div className="col-2  ">
            <button className="btn border col-12  bg-white text-secondary"
                    onClick={()=>this.setState({state:"commande"})}>
              <i className="fa    fa-clock-o" ></i>
            </button>
          </div>
          <div className="col-2">
            <button className="btn border col-12  bg-white text-secondary"
                    onClick={()=>this.setState({state:"validée"})}>
              <i className="fa    fa-arrow-down" ></i>
            </button>
          </div>
          <div className="col-2">
            <button className="btn border col-12  bg-white text-secondary"
                    onClick={()=>this.setState({state:"envoyée"})}>
              <i className="fa    fa-arrow-up" ></i>
            </button>
          </div>
          <div className="col-2">
            <button className="btn border col-12  bg-white text-secondary"
                    onClick={()=>this.setState({state:"reçue"})}>
              <i className="fa    fa-check" ></i>
            </button>
          </div>
          <div className="col-2">
            <button className="btn border col-12  bg-white text-secondary"
                    onClick={()=>this.setState({state:"clôturée"})}>
              <i className="fa  fa-stop  " ></i>
            </button>
          </div>
          <div className="col-2">
            <button className="btn border col-12  bg-white text-secondary"
                    onClick={()=>this.setState({state:""})}>
              <i className="fa  fa-times  " ></i>
            </button>
          </div>
        </div>
      </div>
      {this.state.order &&
      <div className="col-11 mb-4 " >
        <i className="fa fa-arrow-circle-left fa-2x pointer float-right"
          onClick={()=>this.setState({order:false})}
          ></i>
      </div>}

     {/* <Query
      query={GET_ORDERS}
      variables={variables}
      fetchPolicy={"cache-and-network"}
      //variables={{id : ["CJ2gth3WxgyQx2DCv"]  }}
      //pollInterval={2000}
      >
      {({ loading, error, data, variables }) => {
        if (loading) return <p>Loading...</p>;
        if (error) return <p>Error :(</p>;
          // console.log(data);
          // console.log(variables);
          return null;
      }}
      </Query> */}
     <Query
      query={GET_ORDERS}
      variables={variables}
      fetchPolicy={"cache-and-network"}
      //variables={{id : ["CJ2gth3WxgyQx2DCv"]  }}
      //pollInterval={2000}
      >
      {({ loading, error, data, variables }) => {
        // console.log(variables);
        if (loading) return <p>Loading...</p>;
        if (error) return <p>Error :(</p>;
        //random image remove for product
        //console.log(data.getMyOrders.length);
        if(!data.getMyOrders.length){
          return <div className="col-8 mx-auto alert-warning mt-5 p-5 text-center">{formatMessage(messages.content2)}</div>;
        }
        let url = Meteor.absoluteUrl();
        url = url.split('://');
        url[1] = url[1].split(':');
        const baseurl = url[1][0].replace(/\/$/, ""); // remove trailing slash;

        url  = url[0]+"://"+baseurl+':3080/';

        //console.log(data.getMyOrders);
        //const orders =  _.groupBy(data.getItems,"order_id");
        const orders =   data.getMyOrders;
         //console.log(orders);
        //const orders= data.getMyOrders;

        //return Object.keys(orders).map((orderKey,index) => {
        return  orders.map((order,index) => {
          //let img= "/assets/images/"+Math.floor((Math.random() * 4) + 1)+".jpeg";
          //const order = orders[orderKey];
          //console.log(order);
    return (!order.items.length && !this.state.order  )? '':<div key= {index} className="col-12 mb-1 ">
            <a href={"#"} className="no-deco"   onClick={()=>this.setState({order : order._id })} >
            <div className="row col-12 pointer border-bottom  d-flex align-items-center p-1  " >
              {/* <div className="col-2 p-0">
                <img className="col-12 image-fluid p-0" src={url+product._id+"/"+product.filenames[0]}/>
              </div> */}
              <div className="  col-5 pt-2 pb-2 ">{formatMessage(messages.order)} n° {order._id}</div>
              <div className="col-3 pt-2 pb-2 ">{moment(order.date).format('DD MMM YYYY')}</div>
              <div className="col-2 pt-2 pb-2 ">{formatMessage(messages.productsCount)}  {order.items.length}</div>
              <div className="col-2 pt-2 pb-2 ">{formatMessage(messages.total)}  {order.cost}</div>

              {/* <div  className="col-3 d-flex align-items-center">
                <div className="float-right">
                <i data-id={product._id} className={" fa   fa-wish pointer  "+(this.props.whishList.indexOf(product._id)=== -1 ? " fa-heart-o":" fa-heart wish-color" )} onClick={event => {event.preventDefault(); this.props.handleWish(event.target.dataset.id)}}></i>
                <i data-id={product._id} className= {" text-right fa  fa-2x pointer "+(this.props.shopList.indexOf(product._id)=== -1? " fa-shopping-cart ":" fa-cart-arrow-down cart-color")}  onClick={event => {event.preventDefault(); this.props.handleShop(event.target.dataset.id,product.price)}}></i>
                </div>
              </div> */}
            </div>
          </a>


            <div className="row " id={order._id}>
              { this.state.order &&
                order.items.map((item,index)=>(
                  <div key= {index} className="row col-12 mb-1 border-bottom p-4 d-flex align-items-center">
                    <div className="col-1 p-0">
                      <img className="col-12 image-fluid p-0" src={url+item.product_id+"/"+item.img}/>
                    </div>
                    <div  className=" col-5 pl-3">{item.name}</div>
                    <div  className="col-2 p-0"><label>{formatMessage(messages.quantity)}</label> {item.qty}</div>
                    <div  className="col-2 p-0"><label>{formatMessage(messages.price)}</label> {item.cost}</div>
                    {/* <div  className="col-3 p-0"> {formatMessage(messages[item.state])}</div> */}
                    {item.state== "reçue" && <div  className="col-2 p-0">  {formatMessage(messages[item.state])} <i className="fa  fa-check text-warning" ></i></div>}
                    {item.state== "envoyée" &&
                    <Mutation
                          mutation={SET_ORDER}
                          // variables={variables}
                          onCompleted={ data => {

                            this.setState({state: data.setOrderItem.state});
                            }
                          }
                          // update={(cache, { data: { addProduct } }) => {
                          //    //console.log(addProduct);
                          //    if(cache.data.data.ROOT_QUERY.getProducts){
                          //         //console.log(cache.data.data.ROOT_QUERY);
                          //         const products  = cache.readQuery({ query: GET_PRODUCTS });
                          //         //console.log(products);
                          //         cache.writeQuery({
                          //           query: GET_PRODUCTS,
                          //           data: { getProducts: products.getProducts.concat([addProduct]) }
                          //         });
                          //    }
                          //  }}
                           >
                          {(setOrder, { loading, error,data }) => {
                            return <div  className="col-2 p-0">  {formatMessage(messages[item.state])} <i className="fa  text-primary fa-arrow-up" ></i>
                             <button className="btn btn-danger float-right"
                               onClick={()=>{setOrder({variables:{id:item._id, state:item.state}})}}
                               >reçue <i className="fa  fa-angle-double-down"></i></button></div>
                          }}
                    </Mutation>}
                    {item.state== "clôturée" && <div  className="col-2 p-0">  {formatMessage(messages[item.state])} <i className="fa  fa-stop text-info" ></i></div>}
                    {item.state!= "clôturée" && item.state!= "reçue" && item.state!= "envoyée" && <div  className="col-2 p-0">  {formatMessage(messages[item.state])} <i className={"fa  "+(item.state == "commande" ? "text-danger fa-clock-o": (item.state == "validée" ? "text-success fa-arrow-down":"text-primary fa-arrow-up")) } ></i></div>}

                  </div>
                ))
              }
            </div>
          </div>

        });
      }}
    </Query>
    </div>

  }

}

export default injectIntl(Purchases);
