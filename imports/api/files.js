import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Promise } from 'meteor/promise';
import  fs  from 'fs';
export const Files = new Mongo.Collection('files');

//console.log(Files.find().fetch());

if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'files.insert'(productId,filename,data) {

    const id = Files.insert({productId,filename,data});
    console.log(id);
    //Files.insert(product);
    return id;
  },
  'files.update'(product) {


     Files.update({_id: product._id},{name : product.name});
    //return product._id;
  },
  'files.remove'(productId,filename) {

    const UPLOAD_DIR = Meteor.isProduction? "/uploads":"/var/www/rural/uploads";
    try {
          fs.unlinkSync(`${UPLOAD_DIR}/${productId}/${filename}`);
          console.log('successfully deleted  '+`${UPLOAD_DIR}/${productId}/${filename}`);
          Meteor.call('products.removeFileName',productId,filename);
          return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  },


});



}
