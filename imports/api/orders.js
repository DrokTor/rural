import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Random } from 'meteor/random'
import { check } from 'meteor/check';
import { Products } from './products.js';
export const Orders = new Mongo.Collection('orders');
export const OrdersItems = new Mongo.Collection('orders_items');

//console.log(Orders.find().fetch());

if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'orders.insert'(order,user) {

    // {_id: "uyshkjqsoidqs" ,
    //  pid: 'odoiuqsidy',
    //   sellerId:'iudjqsdkjqsdj',
    //    buyerId: "dqsùdlkqlksdl",
    //     qty: "3",
    //      orderId: "oiduqsidu",
    //      state: "New",
    //      error : "",
    //    }
    // {
    //   buyer_id: user._id,
    //   state: 'newOrder',
    //   date: new Date(),
    //   items: [
    //     {seller_id: seller_id,qty:"3",error:""},
    //     {seller_id: seller_id,qty:"3",error:""},
    //   ]
    // }
    //const order_id = Random.id();
    let cost=0;
    const order_id = Orders.insert({
      buyer_id: user._id,
      state: 'commande',
      date: new Date(),
      items: [],
    })
    console.log(order.shopList);
    const shopList = order.shopList;
     shopList.map(({_id,qty},index)=>{
      const product = Products.findOne({_id});
      //console.log(Products.findOne({_id: product_id}).user_id);
      if(product.user_id){
        // Orders.insert({
        //   product_id,
        //   seller_id: seller_id,
        //   error: "",
        // })
        //console.log(shop);
        const item_cost=product.price*qty;
        cost+=item_cost;
        const item = {
          order_id: order_id,
          product_id:  _id,
          name: product.name,
          img: product.filenames[0],
          buyer_id: user._id,
          seller_id: product.user_id,
          qty:  qty,
          cost: item_cost,
          state: "commande",
          date: new Date(),
          error: "",
        }

        OrdersItems.insert(item);
        Orders.update({_id:order_id},{$set:{cost}});

      }else {
        // manage scenario where some seller_id or products not found, inform customer
        console.log("ERROR : seller_id not found.");
        return false;
      }

    })
    // [{pid: 'ddd', seller:'eeee'}]
    // const id = Orders.insert({
    //   state: 'newOrder',
    //   user_id: user._id,
    // })
    // console.log( user);
    // console.log(order);
    //Orders.insert(order);
    return true;
  },
  'orders.updateFileName'(id, filenames) {
    //console.log(id , filename);
     Orders.update({_id:  id},{$addToSet : {filenames}});
  },
  'orders.updateState'(order,user) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })

    const state = order.state=="commande" ? "validée":(order.state=="validée" ? "envoyée":(order.state=="envoyée" ? "reçue":"clôturée")) ;
    //const update = Orders.update({_id: order._id,'items.product_id': order.product_id},{ $set : {'items.$.state' : state}});
    const update = OrdersItems.update({_id: order._id },{ $set : {'state' : state}});
    //console.log(update);
    if(update){
      let item = OrdersItems.findOne({_id: order._id});
      item.state = state;
      return item;
    }else {
      console.log(update);
      return false;
    }
  },
  'orders.remove'(orderID) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Orders.remove({_id: orderID});
    //return order._id;
  },
  'orders.showId'() {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })


    //console.log(this.userId);


  },

});


  // This code only runs on the server
  // Meteor.publish('order', function orderPublication(userId='',search='') {
  //   let searchArray=[];
  //   searchArray.push({ ref: new RegExp(  search + ".*", 'i' ) });
  //   searchArray.push({ des: new RegExp(  search + ".*", 'i' ) });
  //   let query = {$or:searchArray};
  //   userId ? query['user_id']=userId:'';
  //   //console.log(query);
  //   //console.log(searchArray);
  //   //return Orders.find(search?{ user_id:(userId? userId :'' ), $text: {$search: search}}:{}, {score: {$meta: "textScore"}}, {$sort : {score:{$meta:"textScore"}} }) ;
  //   return Orders.find(query);
  // });
}
