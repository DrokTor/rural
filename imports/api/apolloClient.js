import { Accounts } from 'meteor/accounts-base'
//import ApolloClient from 'apollo-boost'
import gql from "graphql-tag";
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink, Observable } from 'apollo-link';
import { withClientState } from 'apollo-link-state';
import { createUploadLink } from 'apollo-upload-client';
import { persistCache } from 'apollo-cache-persist';
// apollo boost
// const Client = new ApolloClient({
//   uri: '/graphql',
//   request: operation =>
//     operation.setContext(() => ({
//       headers: {
//         authorization: Accounts._storedLoginToken()
//       }
//     }))
// })


const request = async (operation) => {
  //const token = await AsyncStorage.getItem('token');
  operation.setContext({
    headers: {
      authorization: Accounts._storedLoginToken()//token
    }
  });
};

const requestLink = new ApolloLink((operation, forward) =>
  new Observable(observer => {
    let handle;
    Promise.resolve(operation)
      .then(oper => request(oper))
      .then(() => {
        handle = forward(operation).subscribe({
          next: observer.next.bind(observer),
          error: observer.error.bind(observer),
          complete: observer.complete.bind(observer),
        });
      })
      .catch(observer.error.bind(observer));

    return () => {
      if (handle) handle.unsubscribe();
    };
  })
);

const cache = new InMemoryCache();
const defaultOptions = {
  query: {
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
  },
}
const Client = new ApolloClient({
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) =>
          console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
          ),
        );
      if (networkError) console.log(`[Network error]: ${networkError}`);
    }),
    requestLink,
    withClientState({
      defaults: {
        isConnected: true
      },
      typeDefs:`
        type Item_qty{
          _id: ID
          qty: Int
        }
      `,
      resolvers: {
        Mutation: {
          updateNetworkStatus: (_, { isConnected }, { cache }) => {
            cache.writeData({ data: { isConnected }});
            return null;
          },
          updateShopList: (_, variables, { cache, getCacheKey }) => {
            //const id = getCacheKey({ __typename: 'wishList', id: variables.id })
            const query = gql`
              query GetShopList {
                shopList @client {
                  _id
                  qty
                }
              }
            `;
            const list = [
              {
                "_id": "cgMepLPtvCPvERRPt",
                "qty": 1,
              }
            ]
            const shopList = cache.readQuery({ query });
            console.log(shopList);
            console.log(variables.shopList);
            //const data = { ...shopList, completed: !todo.completed };
            //cache.writeQuery({query,  data: {shopList: list} });
            cache.writeData({data: {shopList:variables.shopList}});
            return null;//variables.shopList;
          },
        }
      },
      //cache
    }),
    createUploadLink({
      uri: '/graphql',
      credentials: 'same-origin'
    }),
    // new HttpLink({
    //   uri: '/graphql',
    //   credentials: 'same-origin'
    // })
  ]),
  cache: cache,
});
  // Set up cache persistence.
  persistCache({
    cache,
    storage: window.localStorage,
  });
export default Client;
