import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Random } from 'meteor/random'
import { check } from 'meteor/check';
import { Products } from './products.js';
export const Ratings = new Mongo.Collection('ratings');

//console.log(Ratings.find().fetch());

if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'ratings.insert'(rating,user) {

    //const rating_id = Random.id();

    const rating_id = Ratings.insert({
      product_id:rating.product_id,
      rating:rating.rating,
      title:rating.title,
      comment:rating.comment,
      user_id: user._id,
      date: new Date(),
      })

    const product = Products.findOne({_id:rating.product_id});

    const lastRatingSum = (product.rating || 0)*(product.ratingCount || 0);
    let newRating = parseFloat(lastRatingSum+rating.rating)/parseFloat( (product.ratingCount || 0)+1); //4.5+5+3+2+5/5 = rating
    console.log(lastRatingSum);
    console.log(newRating);
    Products.update({_id:product._id},{$set: {rating:newRating,ratingCount:(product.ratingCount || 0)+1}});
   return rating_id;
  },
  'ratings.updateFileName'(id, filenames) {
    //console.log(id , filename);
     Ratings.update({_id:  id},{$addToSet : {filenames}});
  },
  'ratings.updateState'(ratings,user) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })

    const state = ratings.state=="commande" ? "validée":(ratings.state=="validée" ? "envoyée":(ratings.state=="envoyée" ? "reçue":"clôturée")) ;
    //const update = Ratings.update({_id: ratings._id,'items.product_id': ratings.product_id},{ $set : {'items.$.state' : state}});
    const update = RatingsItems.update({_id: ratings._id },{ $set : {'state' : state}});
    //console.log(update);
    if(update){
      let item = RatingsItems.findOne({_id: ratings._id});
      item.state = state;
      return item;
    }else {
      console.log(update);
      return false;
    }
  },
  'ratings.remove'(ratingsID) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Ratings.remove({_id: ratingsID});
    //return ratings._id;
  },
  'ratings.showId'() {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })


    //console.log(this.userId);


  },

});



}
