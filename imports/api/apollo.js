import { ApolloServer, gql } from 'apollo-server-express'
import { WebApp } from 'meteor/webapp'
import { getUser } from 'meteor/apollo'
import fs from 'fs';
import pth from 'path';
import { _ } from 'lodash';
import { Accounts } from 'meteor/accounts-base'
import {Products} from './products'
import {Orders} from './orders'
import {OrdersItems} from './orders'
import {Ratings} from './ratings'
import {Files} from './files'
// import typeDefs from './schema'
// import resolvers from './resolvers'

// The GraphQL schema
const typeDefs = gql`
  type Query {
    "A simple type for getting started!"
    getProducts(_id: [ID] , name: String, category: String,family: String, city: String,availability: String, weight: String, price: String, filename: [String], limit: Int, popular: Boolean) : [Product]
    getRatings(product_id: ID , user_id: ID, limit: Int) : [Rating]
    getMyOrders(_id: ID ,buyer: Boolean) : [Order]
    getOrders(_id: ID ,user_id: [ID]) : [Order]
    getItems(order_id: ID, type: String, state: String ) : [Order_Items]
    getUser: User
    checkEmail(email: String!): Boolean
    getProfile(_id: ID): Profile
    uploads: [File]
  }

  type Mutation {
    addProduct(_id: ID, name: String, category: String, family: String, city: String, weight: String, availability: String!,  qtyAvailable: Int, prepTime: Int, price: Int, description: String,files: [Upload!]!): Product
    newOrder(shopList: [ShopList!]!): Boolean
    deleteFile(_id: ID!, name: String!): Boolean
    #setOrder(orderItem_id:ID ,state: String): Order_Items
    setOrderItem(_id:ID ,state: String): Order_Items
    saveProfile(firstName: String!,lastName: String!,birthDate: Date!,country: String!,city: String!,address: String!,phone: String!): Profile
    addRating(product_id: ID!, rating:Int! ,title: String!,comment: String!): Rating
    # uploadFile(file: Upload!, productId: ID!): File!
    # uploadMultipleFiles(files: [Upload!]! ): [File]!
  }
  type File {
    filename: String!
    mimetype: String!
    encoding: String!
  }
  input ShopList {
    _id: ID!
    qty: String!
  }
  type Item_qty{
    _id: ID
    qty: Int
  }
  type Rating{
    _id: ID
    product_id: ID
    rating: Int!
    title: String!
    comment: String!
  }
  type Profile{
    firstName: String!
    lastName: String!
    birthDate: Date!
    country: String!
    city: String!
    address: String!
    phone: String!
  }
  type Product {
    "A product posted by a seller"
    _id: ID,
    name: String,
    city: String,
    category: String,
    family: String,
    availability: String,
    qtyAvailable: Int,
    prepTime: Int,
    weight: String,
    price: Int,
    filenames: [String],
    rating: String,
    description: String,
    ratings: [Rating],
  }

  scalar Date

  type Order {
    "An order posted by a buyer"
    _id: ID,
    buyer_id: ID,
    date:   Date ,
    cost: String,
    items(order_id: ID, type: String, state: String ): [Order_Items],

  }

  type Order_Items{
    _id: ID,
    seller_id: ID,
    order_id: ID,
    product_id: ID,
    product_name: String,
    name: ID,
    img: ID,
    buyer_id: ID,
    date: Date,
    qty: String,
    cost: String,
    state: String,
    error: String,
  }

  type User {
    "A user, can be a seller or a buyer"

    username : String,
    email : String,
    type : String,
  }
`;

// A map of functions which return data for the schema.
const resolvers = {
  Order:{
    items: (root,args,context) => {

      let search = {order_id:root._id};
      args.state? search.state = args.state:'';
      if(args.type){
        search.buyer_id = context.user._id;
      }else {
        search.seller_id = context.user._id;
      }
      //const search = args.type == "buyer" ? {buyer_id: context.user._id}:{seller_id: context.user._id};
      const items = OrdersItems.find(search).fetch();
      return items;
    }
  },
  Product:{
    ratings: (root,args,context) => {

      let search = {product_id:root._id};
      // console.log('search');
      // console.log(search);

      const ratings = Ratings.find(search,{sort : {date : -1}}).fetch();
      // console.log("ratings");
      // console.log(ratings);
      return ratings;
    }
  },
  Order_Items:{
    product_name: (root,args,context) => {

      let search = {_id:root.product_id};
       // console.log(root);
       // console.log("root");
      // console.log(search);

      const product = Products.findOne(search);

      return product.name;
    }
  },
  Query: {
    getProducts: (root,args,context) => {
      //console.log(Object.keys(args).length );
      //console.log(args);
      let search= {};
      let options= {limit:  args.limit};
      if(args._id) search._id = {$in : args._id};
      if(args.user_id) search.user_id =  context.user._id;
      if(args.category && args.category!="null") search.category =  args.category;
      if(args.family && args.family!="null") search.family =  args.family;
      if(args.availability && args.availability!="null") search.availability =  args.availability;
      if(args.price ) search.price =  {$gte : parseFloat(args.price.split('-')[0]) , $lte : parseFloat(args.price.split('-')[1]) };
      if(args.popular ) options.sort =  {rating: -1};
      // console.log(search);
      // console.log(Products.find(search ,{limit:  args.limit}).fetch());
      if(Object.keys(args).length >0)
        return  Products.find(search ,options).fetch();
      else
      return  Products.find({},options).fetch();
    },
    getRatings: (root,args,context) => {
      //console.log(Object.keys(args).length );
      // console.log("ratings");
      // console.log(args);
      let search= {};

      if(args.user_id) search.user_id =  context.user._id;
      if(args.top) search.user_id =  context.user._id;
      //console.log(Ratings.find(search ,{sort: { date: -1,rating: -1},limit:  args.limit}).fetch());
      if(Object.keys(args).length >0)
        return  Ratings.find(search ,{sort: { date: -1,rating: -1},limit:  args.limit}).fetch();
      else
      return  Ratings.find({},{sort: {rating: -1, date: -1}, limit:  args.limit}).fetch();
    },
    getMyOrders (root,args,context){
      //console.log(args);
      let search = args._id? {_id:args._id }:{};
      if(args.buyer){
        search.buyer_id = context.user._id
      }else {
        search['items.seller_id'] = context.user._id
      }
      let orders = Orders.find(search).fetch();
      //console.log(search);
      return orders ;
    },
    getItems (root,args,context)   {
      //console.log(args);
      // let search = args._id? {_id:args._id }:{};
      // if(args.buyer){
      //   search.buyer_id = context.user._id
      // }else {
      //   search['items.seller_id'] = context.user._id
      // }
      // let orders = Orders.find(search).fetch();
      let search = args.order_id? {order_id:args.order_id }:{};
      args.state? search.state = args.state:'';
      if(args.type){ // if type is set explecitly to buyer, get buyer's item, otherwise implcitly get seller's items
        search.buyer_id = context.user._id
      }else {
        search.seller_id = context.user._id
      }

      //const search = args.type == "buyer" ? {buyer_id: context.user._id}:{seller_id: context.user._id};
      let orders = OrdersItems.find(search).fetch();
      //console.log(orders);
      return orders ;
    },
    getUser: (root,args,context) => {
      //console.log(args);
        return context.user && {email : context.user.emails[0].address};
    },
    getProfile: (root,args,context) => {
       console.log(args);
       if(args._id){
         console.log(Meteor.users.findOne({_id: args._id}).profile);
         return Meteor.users.findOne({_id: args._id}).profile;
       }else{
         return context.user && context.user.profile;
       }
    },
    checkEmail: (root,args,context) => {
      const user = Accounts.findUserByEmail(args.email);
       // console.log();
       // console.log(args);
       // console.log('check email');
       if(user){
         return user.emails[0].verified;
       }else {
         return null;
       }

    },
    // files: () => {
    //   // Return the record of files uploaded from your DB or API or filesystem.
    // },
  },
  Mutation: {
    async addProduct(root,args,context) {
      //console.log(context.user);
      if(context.user)
      {//console.log(args);
        if(args._id){ // edit & update
          Meteor.call('products.update',args,context.user);
        }else{ // insert
          const id = Meteor.call('products.insert',args,context.user);
          args._id = id;
        }
        //console.log(id);
        args.filenames = [];
        //const { stream, filename, mimetype, encoding } = await args.file;
        args.files.map( async ( file, index) => {
          let { stream, filename, mimetype, encoding } = await file;
          args.filenames.push(filename);
          Meteor.call('products.addFileName',args._id,filename);
          //const st = fs.createReadStream(stream);
           //console.log(filename);

           const   path  = await storeFS({ stream, filename, productId: args._id })
        })


        return args;
      }else {
        return context;
      }
    },
    async newOrder(root,args,context) {
      //console.log(context.user);

      if(context.user)
      {//console.log(args);
       //console.log('args');
        const id = Meteor.call('orders.insert',args,context.user);

        return id || true;
      }else {
        return context;
      }
    },
    async deleteFile(root,args,context) {
      //console.log(context.user);

      if(context.user)
      {//console.log(args);
       //console.log('delete file');
       const res = Meteor.call('files.remove',args._id,args.name);

        return res || true;
      }else {
        return context;
      }
    },
    async setOrderItem(root,args,context) {
      //console.log(context.user);

      if(context.user)
      { //console.log(args);
        //const id =
        const update = Meteor.call('orders.updateState',args,context.user);
        //console.log(update);
        //console.log(args);
        return  update;
      }else {
        return context;
      }
    },
    async addRating(root,args,context) {
      //console.log(context.user);
      //console.log(args);
      if(context.user)
      { //console.log(args);
        //const id =
        const id = Meteor.call('ratings.insert',args,context.user);
          // console.log(id);
          // console.log(args);
        return  id && args;
      }else {
        return context;
      }
    },
    async saveProfile(root,args,context) {
      //console.log(context.user);
      //console.log(args);
      if(context.user)
      { //console.log(args);
        //const id =
        //const id = Meteor.call('ratings.insert',args,context.user);
          // console.log(id);
          // console.log(args);
          Meteor.users.update({_id: context.user._id},{$set : {profile: args}});
        return   args;
      }else {
        return context;
      }
    },
    // async uploadMultipleFiles(parent, { files }) {
    //   console.log(files);
    //   files.map( async ( file, index) => {
    //     let { stream, filename, mimetype, encoding } = await file;
    //
    //     //const st = fs.createReadStream(stream);
    //      console.log(filename);
    //
    //      const   path  = await storeFS({ stream, filename, productId: id })
    //   })
    //
    //   return files;
    // },
    // async uploadFile(parent, { file , productId }) {
    //   const { stream, filename, mimetype, encoding } = await file;
    //
    //     //const st = fs.createReadStream(stream);
    //    console.log(filename);
    //    const   path  = await storeFS({ stream, filename, productId })
    //    //return storeDB({ id, filename, mimetype, path })
    //    console.log('path');
    //   return { filename, mimetype, encoding };
    // }
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => ({
    user: await getUser(req.headers.authorization)
  })
})

server.applyMiddleware({
  app: WebApp.connectHandlers,
  path: '/graphql'
})

WebApp.connectHandlers.use('/graphql', (req, res) => {
  if (req.method === 'GET') {
    res.end()
  }
})


////////////////////////
////////////////////////

const UPLOAD_DIR = Meteor.isProduction? "/uploads":"/var/www/rural/uploads";
//const UPLOAD_DIR = "/tmp/rural/uploads";
//const UPLOAD_DIR = process.env.PWD;//+ "/.uploads";

const  storeFS = ({ stream, filename , productId }) => {
  //const id = shortid.generate()
  // console.log(process.env.PWD);
  // console.log('..'+process.env.PWD);
  // console.log('../..'+process.env.PWD);
  // console.log('../../..'+process.env.PWD);
  // console.log(pth.resolve('../../../..'+process.env.PWD));
  // console.log(process.cwd());
  // console.log(__dirname);
  //console.log(productId);


  // fs.access(`${UPLOAD_DIR}/${productId}`, fs.constants.F_OK, (err) => {
  //   if (err) {
  //     fs.mkdir(`${UPLOAD_DIR}/${productId}`, { recursive: true }, (err) => {
  //       if (err) throw err;
  //     });
  //   }
  // });

  try {
    fs.accessSync(`${UPLOAD_DIR}/${productId}/`,   fs.constants.W_OK);
    //fs.accessSync(`${UPLOAD_DIR}/${productId}/`,   fs.constants.W_OK);

  } catch (err) {
    //console.log(err);
    try {
      // fs.mkdirSync(`${UPLOAD_DIR}/` , (err) => {
      // //fs.mkdirSync(`${UPLOAD_DIR}/${productId}/`, { recursive: true }, (err) => {
      //   if (err) console.log(err);
      // })
      fs.mkdirSync(`${UPLOAD_DIR}/${productId}/`, { recursive: true } , (err) => {
      //fs.mkdirSync(`${UPLOAD_DIR}/${productId}/`, { recursive: true }, (err) => {
        if (err) console.log(err);
      })

    } catch (e) {
      console.log(e);
    }
  }



  //const path = `${UPLOAD_DIR}//${filename}`
  const path = `${UPLOAD_DIR}/${productId}/${filename}`
  return new Promise((resolve, reject) =>
    stream
      .on('error', error => {
        if (stream.truncated)
          // Delete the truncated file
          fs.unlinkSync(path)
        reject(error)
      })
      .on('data', Meteor.bindEnvironment( (data,err) => {
        // do stuff
        // can access Meteor.userId
        // still have MongoDB write fence
        // Meteor.call('files.insert',productId,filename,data.toString('base64'),(err,res)=>{
        //   //console.log(res );
        // })
        //console.log(data);
      } )
      //  data => {
      //   //console.log(new Buffer(data));
      //   //console.log(data.toString('base64'));
      //   // Meteor.wrapAsync(Meteor.call('files.insert',filename,data,(res,err)=>{
      //   //   console.log('kkk');
      //   // }))
      //   //const asyncf = Meteor.wrapAsync(Meteor.call('files.insert'));
      //
      //   //console.log(asyncf(filename,data));
      // }
      )
      //.pipe( console.log('ttt'))
      .pipe(fs.createWriteStream(path))
      .on('error', error => reject(error))
      .on('finish', ( ) => {console.log('finished');resolve({  path })} )
  )
}

// const processUpload = async upload => {
//   const { createReadStream, filename, mimetype } = await upload
//   const stream = createReadStream()
//   const { id, path } = await storeFS({ stream, filename })
//   return storeDB({ id, filename, mimetype, path })
// }
