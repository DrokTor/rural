import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Products = new Mongo.Collection('products');

//console.log(Products.find().fetch());

if (Meteor.isServer) {

Meteor.methods({
  //receive request
  'products.insert'(product,user) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })


    // console.log(Meteor.userId());
    // // Make sure the user is logged in before inserting a task
    // if (! this.userId) {
    //   throw new Meteor.Error('not-authorized');
    // }
    //product.user_id = this.userId;
    //product.store = Meteor.users.findOne(this.userId).profile.store;
    //product.name = this.userId;
    //console.log(product.city);
    const id = Products.insert({
      name: product.name,
      city: product.city,
      category: product.category,
      family: product.family,
      availability: product.availability,
      qtyAvailable: product.qtyAvailable,
      prepTime: product.prepTime,
      weight: product.weight,
      price: product.price,
      description: product.description,
      user_id: user._id,
    })
    //Products.insert(product);
    return id;
  },
  'products.addFileName'(id, filenames) {
    //console.log(id , filename);
     Products.update({_id:  id},{$addToSet : {filenames}});
  },
  'products.removeFileName'(id, filenames) {
    console.log(id , filenames);
     const up = Products.update({_id:  id},{ $pull: { filenames } });
     console.log(up);
  },
  'products.update'(product) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })
    const id = Products.update(
      {_id: product._id},
      { $set :
        {
        name: product.name,
        city: product.city,
        category: product.category,
        family: product.family,
        availability: product.availability,
        qtyAvailable: product.qtyAvailable,
        prepTime: product.prepTime,
        weight: product.weight,
        price: product.price,
        description: product.description,
        //user_id: user._id,
        }
      })
  },
  'products.remove'(productID) {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })




    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

     Products.remove({_id: productID});
    //return product._id;
  },
  'products.showId'() {
    // check(client,{
    //   id : String,
    //   name : String,
    //   ip : String,
    // })


    //console.log(this.userId);


  },

});


  // This code only runs on the server
  // Meteor.publish('product', function productPublication(userId='',search='') {
  //   let searchArray=[];
  //   searchArray.push({ ref: new RegExp(  search + ".*", 'i' ) });
  //   searchArray.push({ des: new RegExp(  search + ".*", 'i' ) });
  //   let query = {$or:searchArray};
  //   userId ? query['user_id']=userId:'';
  //   //console.log(query);
  //   //console.log(searchArray);
  //   //return Products.find(search?{ user_id:(userId? userId :'' ), $text: {$search: search}}:{}, {score: {$meta: "textScore"}}, {$sort : {score:{$meta:"textScore"}} }) ;
  //   return Products.find(query);
  // });
}
