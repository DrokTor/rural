import { Meteor } from 'meteor/meteor';
import { Files } from "../imports/api/files.js";
import { Accounts } from "meteor/accounts-base";


import  "../imports/api/users.js";
import  "../imports/api/apollo.js";


Accounts.emailTemplates.verifyEmail = {
  from(){
    return "Rural platform";
  },
  subject() {
    return "Vérification de votre addresse mail";
  },
  html(user, url) { //${user}
    const urlSplit = url.split('/#/verify-email/');
    const confUrl = urlSplit[0]+(Meteor.isProduction? ':5007': '')+'/user/verifyEmail?t='+urlSplit[1];
    return `Merci de s'être enregistré ! <br/> Cliquer sur ce lien pour vérifier votre addresse mail:<br/> ${confUrl} `; //.... ${url}
  }
};

Accounts.emailTemplates.resetPassword = {
  from(){
    return "Rural platform";
  },
  subject() {
    return "Réinitialisation de votre mot de passe";
  },
  html(user, url) { //${user}
    const urlSplit = url.split('/#/reset-password/');
    const resetUrl = urlSplit[0]+'/user/reset?t='+urlSplit[1];
    return `Une réinitialisation de mot de passe a été initié pour votre compte. <br/> Cliquer sur ce lien pour réinitialiser votre mot de passe:<br/> ${resetUrl}`;
  }
};

Meteor.startup(() => {
  // code to run on server at startup

  process.env.MAIL_URL = 'smtp://optidevdz:***REMOVED***@smtp.gmail.com:587';
  //process.env.MAIL_URL = 'smtp://optidevdz:***REMOVED***@smtp.gmail.com:587';
});
