import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import AppWrapper from '/imports/ui/AppWrapper'
import WebFont from 'webfontloader';



Meteor.startup(() => {
  WebFont.load({
    google: {
      families: ['Raleway:400,500,700', 'sans-serif']
    }
  });

  render(<AppWrapper />, document.getElementById('react-target'));
});
